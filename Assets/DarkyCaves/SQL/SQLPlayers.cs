﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

namespace SQLPlayerInfo
{
    internal class SQLPlayers
    {
        /// <summary>Get the playerName's PlayerID, this will be the ID used in the other databases to identify the player's information</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal static int GetPlayerID(string playerName)
        {
            int playerID = -1;
            var sqlQuery = String.Format("SELECT PlayerID FROM {0} WHERE PlayerName=\"{1}\"", TableName.Players.ToString(), playerName);
            var querier = new SQLQuerier<int>(SQLManager.Instance.ConnectionString);
            return querier.Query(sqlQuery, new IntResultReader());
        }

        /// <summary>Returns the name of the player who's ID this belongs to</summary>
        /// <param name="playerID"></param>
        /// <returns></returns>
        internal static string GetPlayerName(int playerID)
        {
            string sqlQuery = string.Format("SELECT PlayerName FROM {0} WHERE PlayerID=\"{1}\"", TableName.Players.ToString(), playerID);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.Query(sqlQuery, new StringResultReader());
        }

        /// <summary>Check if the PlayerName provided exists in the Players table in the PlayerInformation database</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal static bool CheckPlayerExistsInDB(string playerName)
        {
            string sqlQuery = String.Format("SELECT PlayerName FROM {0} WHERE PlayerName=\"{1}\"", TableName.Players.ToString(), playerName);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            var name = querier.Query(sqlQuery, new StringResultReader());
            return name != null && name.Equals(playerName);
        }

        /// <summary>Return a list of all PlayerName's which have been added to the database</summary>
        /// <returns></returns>
        internal static IEnumerable<string> GetAllPlayers()
        {
            string sqlQuery = "SELECT PlayerName FROM " + TableName.Players.ToString();
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.QueryMultiple(sqlQuery, new StringResultReader());
        }
    }
}