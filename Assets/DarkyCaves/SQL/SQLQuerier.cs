﻿using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLPlayerInfo
{
    public class SQLQuerier<T>
    {
        private readonly string _connectionString;

        public SQLQuerier(string connectionString)
        {
            _connectionString = connectionString;
        }

        public T Query(string sql, ResultReader<T> resultReader)
        {
            T result;
            using (SqliteConnection connection = new SqliteConnection(_connectionString))
            {
                connection.Open();
                using (var datareader = createCommand(connection, sql).ExecuteReader())
                {
                    result = resultReader.ReadFromDataReader(datareader);
                    connection.Close();
                    datareader.Close();
                }
            }
            return result;
        }

        public IEnumerable<T> QueryMultiple(string sql, ResultReader<T> resultReader)
        {
            IEnumerable<T> result;
            using (SqliteConnection connection = new SqliteConnection(_connectionString))
            {
                connection.Open();
                using (var datareader = createCommand(connection, sql).ExecuteReader())
                {
                    result = resultReader.ReadMultipleFromDataReader(datareader);
                    connection.Close();
                    datareader.Close();
                }
            }
            return result;
        }

        SqliteCommand createCommand(SqliteConnection connection, string sql)
        {
            return new SqliteCommand(sql, connection);
        }
    }
}
