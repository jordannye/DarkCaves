﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;

namespace SQLPlayerInfo
{
    internal class SQLFistingPowers
    {
        internal static IEnumerable<string> GetPlayerUnlockedFistingPowers(string playerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            string sqlQuery = string.Format("SELECT FistingPowerName FROM {0} WHERE PlayerID = \"{1}\"", TableName.UnlockedFistingPowers.ToString(), playerID);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.QueryMultiple(sqlQuery, new StringResultReader()).Where(s => !string.IsNullOrEmpty(s));
        }

        internal static IEnumerable<string> GetPlayerActiveFistingPowers(string playerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            string sqlQuery = string.Format("SELECT FistingPowerName FROM {0} WHERE PlayerID = \"{1}\"", TableName.ActiveFistingPowers.ToString(), playerID);

            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.QueryMultiple(sqlQuery, new StringResultReader()).Where(s => !string.IsNullOrEmpty(s));
        }

        internal static bool AddFistingPower(string playerName, string fistingPowerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            //Create new blank player
            if (!SQLPlayers.CheckPlayerExistsInDB(playerName)) return false;

            using (SqliteConnection connection = new SqliteConnection(SQLManager.Instance.ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    if (!SQLPlayers.CheckPlayerExistsInDB(playerName))
                    {
                        connection.Close();
                        return false;
                    }

                    string sqlAddPassive = String.Format("INSERT INTO {0} (PlayerID, FistingPowerName) VALUES('{1}', '{2}')",
                        TableName.ActiveFistingPowers.ToString(), playerID, fistingPowerName);
                    command.CommandText = sqlAddPassive;
                    command.ExecuteScalar();
                }
                connection.Close();
            }
            return true;
        }

        internal static bool RemoveFistingPower(string playerName, string fistingPowerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            //Create new blank player
            if (!SQLPlayers.CheckPlayerExistsInDB(playerName)) return false;

            using (SqliteConnection connection = new SqliteConnection(SQLManager.Instance.ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    if (!SQLPlayers.CheckPlayerExistsInDB(playerName))
                    {
                        connection.Close();
                        return false;
                    }

                    string sqlAddPassive = String.Format("DELETE FROM {0} WHERE PlayerID = '{1}' AND FistingPowerName = '{2}'",
                        TableName.ActiveFistingPowers.ToString(), playerID, fistingPowerName);
                    command.CommandText = sqlAddPassive;
                    command.ExecuteScalar();
                }
                connection.Close();
            }
            return true;
        }

    }
}
