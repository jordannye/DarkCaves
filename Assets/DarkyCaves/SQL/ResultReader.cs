﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SQLPlayerInfo
{
    public abstract class ResultReader<T>
    {
        public T ReadFromDataReader(IDataReader datareader)
        {
            T result = defaultValue();
            if (datareader.Read())
                result = read(datareader);
            return result;
        }

        public IEnumerable<T> ReadMultipleFromDataReader(IDataReader datareader)
        {
            var list = new List<T>();
            //While reading is possible
            while (datareader.Read())
            {
                //Grab all of the columns that this query has returned, and loop through them
                for (int i = 0; i < datareader.FieldCount; i++)
                {
                    list.Add(read(datareader, i));
                }
            }
            datareader.Close();
            return list;
        }

        protected abstract T read(IDataReader datareader, int index = 0);
        protected abstract T defaultValue();
    }

    public class StringResultReader : ResultReader<string>
    {
        protected override string defaultValue()
        {
            return string.Empty;
        }

        protected override string read(IDataReader datareader, int index = 0)
        {
            return datareader.GetString(index);
        }
    }

    public class BoolResultReader : ResultReader<bool>
    {
        protected override bool defaultValue()
        {
            return false;
        }

        protected override bool read(IDataReader datareader, int index = 0)
        {
            return datareader.GetBoolean(index);
        }
    }

    public class IntResultReader : ResultReader<int>
    {
        protected override int defaultValue()
        {
            return -1;
        }

        protected override int read(IDataReader datareader, int index = 0)
        {
            return datareader.GetInt32(index);
        }
    }

}
