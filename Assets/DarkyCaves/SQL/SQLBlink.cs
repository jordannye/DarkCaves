﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;

namespace SQLPlayerInfo
{
    internal class SQLBlink
    {
        /// <summary>Return the name of the component currently being used as the Blink Ability for the player</summary>
        /// <param name="playerID"></param>
        /// <returns></returns>
        internal static string GetPlayerActiveBlink(string playerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            var sqlQuery = string.Format("SELECT BlinkName FROM {0} WHERE PlayerID = \"{1}\"", TableName.ActiveBlinks.ToString(), playerID);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.Query(sqlQuery, new StringResultReader());
        }

        /// <summary>Get the list of all Unlocked Blink abilities the player has earnt</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal static IEnumerable<string> GetPlayerUnlockedBlinks(string playerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            string sqlQuery = String.Format("SELECT BlinkName FROM {0} WHERE PlayerID = \"{1}\"", TableName.UnlockedBlinks.ToString(), playerID);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.QueryMultiple(sqlQuery, new StringResultReader()).Where(s=>!string.IsNullOrEmpty(s));
        }

        /// <summary>Update the current equipped Blink ability for this player</summary>
        /// <param name="playerName"></param>
        /// <param name="blinkName"></param>
        /// <returns></returns>
        internal static bool UpdateBlinkInfo(string playerName, string blinkName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            //Create new blank player
            if (!SQLPlayers.CheckPlayerExistsInDB(playerName)) return false;

            //Using statement automatiicaly disposes IDisposables
            using (SqliteConnection connection = new SqliteConnection(SQLManager.Instance.ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string passiveValue = string.Format("BlinkName = '{0}'", blinkName);

                    string updateSQL = String.Format("UPDATE {0} SET {1} WHERE PlayerID = '{2}'", TableName.ActiveBlinks.ToString(), passiveValue, playerID);
                    command.CommandText = updateSQL;
                    command.ExecuteScalar();
                    connection.Close();
                }
            }
            return true;
        }
    }
}
