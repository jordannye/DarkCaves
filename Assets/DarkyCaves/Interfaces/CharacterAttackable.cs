﻿using DarkyEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// This class shows that the Entity is able to be attacked by a DamageSource
/// </summary>
[RequireComponent(typeof(CharacterStats), typeof(HealthType))]
public class CharacterAttackable : MonoBehaviour, IAttackable, IUpdateable
{
    /// <summary>Character stats reference</summary>
    private CharacterStats CharacterStats;

    /// <summary>The health of this unit</summary>
    public HealthType HealthType;

    void Start()
    {
        CharacterStats = GetComponent<CharacterStats>();
        HealthType = GetComponent<HealthType>();
        HealthType.RegisterHealthType(CharacterStats);

        DarkyEvents.EventManager.TriggerEvent(EventNames.HealthFirstInitialised, HealthType, gameObject.IsMine());
    }

    public bool ApplyDamage(DamageType damageType)
    {
        bool dead = HealthType.ApplyDamage(damageType);
        DarkyEvents.EventManager.TriggerEvent(EventNames.CharacterDamaged, new { DamageType = damageType, HealthType = HealthType }, gameObject.IsMine());
        return dead;
    }

    public void IUpdate(float deltaTime)
    {

    }
}
