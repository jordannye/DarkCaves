﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseBlink : MonoBehaviour, IOnBlink
{
    private void OnEnable()
    {
        GetComponent<UpdateManger>().AddUpdateable(this);
    }
    private void OnDisable()
    {
        GetComponent<UpdateManger>().RemoveUpdateable(this);
    }

    /// <summary>How long this Blink Ability has left before they are allowed to Blink again</summary>
    public float SecondsLeftBeforeBlinkReady { get; set; }

    public virtual float BlinkCooldownSeconds { get { return 0f; } }

    public virtual bool CheckCanBlink(Vector3 directionalVector, CharacterMainComponent characterMainComp)
    {
        return SecondsLeftBeforeBlinkReady <= 0f;
    }

    public void IUpdate(float deltaTime)
    {
        SecondsLeftBeforeBlinkReady -= deltaTime;
    }

    public virtual void OnBlinkFinished(CharacterMainComponent characterTransform, Vector3 blinkedTo)
    {

    }

    public virtual void OnBlinkStarted(CharacterMainComponent characterTransform, Vector3 blinkedFrom)
    {

    }

    public void TriggerBlink(Vector3 blinkVector, CharacterMainComponent characterMainComponent)
    {
        SecondsLeftBeforeBlinkReady = BlinkCooldownSeconds;
        OnTriggerBlink(blinkVector, characterMainComponent);
    }

    /// <summary>Called directly after starting the blink cooldown by the BaseBlink class.
    /// In this method, handle the behaviour implementation for the Triggering of your blink effect</summary>
    /// <param name="blinkVector"></param>
    /// <param name="characterMainComponent"></param>
    public abstract void OnTriggerBlink(Vector3 blinkVector, CharacterMainComponent characterMainComponent);
}
