﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBlink : BaseBlink
{
    protected float _BlinkDistance = 4f;

    public virtual float BlinkDistance
    {
        get { return _BlinkDistance; }
        protected set { _BlinkDistance = value; }
    }

    override public bool CheckCanBlink(Vector3 directionalVector, CharacterMainComponent characterMainComp)
    {
        //Stop the chain of virtualising this method here, because all Teleport blinks will atttempt to teleport into their locations.
        //And collisions will be handled externally to this component.Other Blinks may get stopped by objects

        return base.CheckCanBlink(directionalVector, characterMainComp);
    }

    public override float BlinkCooldownSeconds { get { return 6f; } }

    public override void OnTriggerBlink(Vector3 blinkVector, CharacterMainComponent characterMainComponent)
    {
        //Reduce the length of the blink everytime you detect you want to blink inside the floor, until your blinking 0 distance.
        //This way you can't get stuck inside the floor
        float length = BlinkDistance;
        bool possible = false;
        while (!possible && length > 0)
        {
            var hit = Physics2D.Raycast(transform.position, blinkVector, length, LayerMask.GetMask(new string[] { "Floor", "Obstacles" }));
            possible = hit.collider == null;
            if (!possible) length--;
        }
        transform.position += blinkVector * length;
    }
}
