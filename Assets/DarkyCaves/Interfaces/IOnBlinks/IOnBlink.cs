﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOnBlink : IUpdateable
{
    /// <summary>How long it takes for the blink to cooldown, before its ready to use again</summary>
    float BlinkCooldownSeconds { get; }

    /// <summary>Check to see if it is possible for a blink to be made, this decides how far to blink in the direction given
    /// and the rules that need to be applied to see if the blink is possible</summary>
    /// <param name="directionalVector"></param>
    /// <returns></returns>
    bool CheckCanBlink(Vector3 directionalVector, CharacterMainComponent characterMainComp);

    /// <summary>The request has been made that the Charcter Blink in this direction, apply your logic to blink the chararacter</summary>
    /// <param name="blinkVector"></param>
    /// <param name="characterMainComponent"></param>
    void TriggerBlink(Vector3 blinkVector, CharacterMainComponent characterMainComponent);

    /// <summary>Called when the character started their blink, given the location they blinked from</summary>
    /// <param name="blinkedFrom"></param>
    void OnBlinkStarted(CharacterMainComponent characterTransform, Vector3 blinkedFrom);

    /// <summary>Called when the character blink has finished and you have landed.</summary>
    /// <param name="blinkedTo"></param>
    void OnBlinkFinished(CharacterMainComponent characterTransform, Vector3 blinkedTo);


}
