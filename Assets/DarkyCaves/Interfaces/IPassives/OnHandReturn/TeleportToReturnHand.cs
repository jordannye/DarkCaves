﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportToReturnHand : BasePassive
{
    public override void UserEarlyReturn(ThrowableObject hand, Vector2 pointReturningFrom)
    {
        Vector3 currentPos = hand.GetOwnerCharacter().transform.position;
        hand.GetOwnerCharacter().transform.position = pointReturningFrom;
        _Completed = true;
    }

    public override string ToString()
    {
        return "Teleport User To Hand";
    }
}
