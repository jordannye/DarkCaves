﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultWaitPassive : BasePassive
{
    /// <summary>How long after being activated til the passive deactivates</summary>
    public float SecondsBeforeDeactivatePassive = 1f;
    /// <summary>Current time passed since activation</summary>
    private float CurrentSecondsBeforeDeactivated = 0f;

    public override void OnPassiveActivate(CharacterMainComponent characterMain)
    {
        CurrentSecondsBeforeDeactivated = SecondsBeforeDeactivatePassive;
    }

    public override void IUpdate(float deltaTime)
    {
        base.IUpdate(deltaTime);
        CurrentSecondsBeforeDeactivated -= deltaTime;
    }

    public override bool PassiveCompleted()
    {
        return CurrentSecondsBeforeDeactivated <= 0;
    }

    public override string ToString()
    {
        return "Wait " + SecondsBeforeDeactivatePassive + " seconds passive";
    }
}
