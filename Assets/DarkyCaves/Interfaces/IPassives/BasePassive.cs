﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePassive : MonoBehaviour, IPassive
{
    protected bool _Completed;

    public float PowerPercentageConsumption { get { return 0f; } }

    public float BlinkCooldownSeconds { get { return 0f; } }

    public virtual void BeginReturn(ThrowableObject hand, Vector2 pointReturningFrom)
    {

    }

    public bool CheckCanBlink(Vector3 directionalVector, CharacterMainComponent characterMainComp)
    {
        return false;
    }

    public virtual void HandAttack(ThrowableObject hand, IAttackable attackable, Vector2 attackedPoint)
    {

    }

    public virtual void HandReturned(ThrowableObject hand, Vector2 pointReturnedTo)
    {

    }

    public virtual void IUpdate(float deltaTime)
    {

    }

    public virtual void OnBlinkFinished(CharacterMainComponent characterTransform, Vector3 blinkedTo)
    {

    }

    public virtual void OnBlinkStarted(CharacterMainComponent characterTransform, Vector3 blinkedFrom)
    {

    }

    public virtual void OnPassiveActivate(CharacterMainComponent characterMain)
    {
        _Completed = false;
    }

    public virtual void OnPassiveDeactivate(CharacterMainComponent characterMain)
    {
        _Completed = false;
    }

    public virtual bool PassiveCompleted()
    {
        return _Completed;
    }

    public virtual void StartThrowing(ThrowableObject hand, Vector2 pointThrownFrom)
    {

    }

    public void TriggerBlink(Vector3 blinkVector, CharacterMainComponent characterMainComponent)
    {
        throw new NotImplementedException();
    }

    public virtual void UserEarlyReturn(ThrowableObject hand, Vector2 pointReturningFrom)
    {

    }
}
