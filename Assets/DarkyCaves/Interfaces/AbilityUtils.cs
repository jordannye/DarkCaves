﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>This class wil store all of the names of the passives you can use in the game
/// And it will return the types for each passive as well</summary>
public class AbilityUtils
{
    public class PassiveNames
    {
        public static string DefaultWaitPassive = "DefaultWaitPassive";

        public static string TeleportToReturnHand = "TeleportToReturnHand";
    }

    public static System.Type PassiveType(string passiveName)
    {
        if (passiveName.Equals(PassiveNames.DefaultWaitPassive)) return typeof(DefaultWaitPassive);
        if (passiveName.Equals(PassiveNames.TeleportToReturnHand)) return typeof(TeleportToReturnHand);

        return null;
    }

    public class BlinkNames
    {
        public static string TeleportBlink = "TeleportBlink";

        public static string TeleportSonicBoomLandBlink = "TeleportSonicBoomLandBlink";
    }

    public static System.Type BlinkType(string passiveName)
    {
        if (passiveName.Equals(BlinkNames.TeleportBlink)) return typeof(TeleportBlink);
        if (passiveName.Equals(BlinkNames.TeleportSonicBoomLandBlink)) return typeof(TeleportSonicBoomLandBlink);

        return null;
    }

    public class FistingPowerNames
    {
        public static string SonicExplosionAttackAction = "SonicExplosionAttackAction";
    }

    public static System.Type FistingPowerType(string passiveName)
    {
        if (passiveName.Equals(FistingPowerNames.SonicExplosionAttackAction)) return typeof(SonicExplosionAttackAction);

        return null;
    }
}
