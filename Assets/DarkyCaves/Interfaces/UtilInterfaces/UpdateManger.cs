﻿using DarkyEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateManger : Photon.MonoBehaviour
{
    private bool _PauseUpdate;
    public bool PauseUpdate
    {
        get { return _PauseUpdate; }
        set
        {
            _PauseUpdate = value;
            PlayerController.Instance.CharacterControllerSwitch.PauseUpdate = _PauseUpdate;
            Time.timeScale = _PauseUpdate ? 0f : 1f;
        }
    }

    private void SetPauseUpdate(System.Object pauseUpdate) { PauseUpdate = (bool)pauseUpdate; }

    private List<IUpdateable> UpdateableScripts = new List<IUpdateable>();

    /// <summary>Add this</summary>
    /// <param name="newUpdateable"></param>
    public void AddUpdateable(IUpdateable newUpdateable)
    {
        if (newUpdateable == null || UpdateableScripts.Contains(newUpdateable)) return;
        UpdateableScripts.Add(newUpdateable);
    }

    public void RemoveUpdateable(IUpdateable updateable)
    {
        UpdateableScripts.Remove(updateable);
    }

    private void Awake()
    {
        EventManager.StartListening(EventNames.HubPauseMenu, this.SetPauseUpdate);
    }

    // Use this for initialization
    void Start()
    {
        //Attach all componenets which are IUpdatables, but ignore IPassive's as they will be handled and updated by the CharacterBuffController
        foreach (Component comp in GetComponents<Component>())
        {
            IUpdateable updateable = comp as IUpdateable;
            IPassive ipassve = comp as IPassive;
            if (updateable == null || UpdateableScripts.Contains(updateable) ||
            ipassve != null || UpdateableScripts.Contains(ipassve)) continue;
            UpdateableScripts.Add(updateable);
        }
    }

    private void OnDestroy()
    {
        if (UpdateableScripts != null) UpdateableScripts.Clear();

        EventManager.StopListening(EventNames.HubPauseMenu, this.SetPauseUpdate);
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseUpdate) return;

        foreach (IUpdateable updateable in UpdateableScripts)
        {
            updateable.IUpdate(Time.deltaTime);
        }
    }
}
