﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>If you are inhertting from this class, then you are expecting another MonoBehaviour class to manage the update for you
/// Instead of implementing the method Update() and letting unity reflect it</summary>
public interface IUpdateable
{
    void IUpdate(float deltaTime);
}
