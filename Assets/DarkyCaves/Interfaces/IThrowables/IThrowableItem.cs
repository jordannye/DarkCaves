﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>This class will server as an Interface for any item you wish to attach to a ThrowableHand
/// A list of these components will be attached to every ThrowableHand and upon certain events happening, this classes methods will be called for each ThrowableItem
/// </summary>
public interface IThrowableItem
{
    /// <summary>The % this throwable item will consume upon being equipped</summary>
    float PowerPercentageConsumption { get; }

    /// <summary>This hand has just been thrown</summary>
    /// <param name="hand"></param>
    void StartThrowing(ThrowableObject hand, Vector2 pointThrownFrom);

    /// <summary>This hand was forced to return early by the player</summary>
    /// <param name="hand"></param>
    void UserEarlyReturn(ThrowableObject hand, Vector2 pointReturningFrom);

    /// <summary>This hand as just started its return back to its Character</summary>
    /// <param name="hand"></param>
    void BeginReturn(ThrowableObject hand, Vector2 pointReturningFrom);

    /// <summary>This hand has just returned back to the Character</summary>
    /// <param name="hand"></param>
    void HandReturned(ThrowableObject hand, Vector2 pointReturnedTo);

    /// <summary>This hand has hit a target</summary>
    /// <param name="hand"></param>
    void HandAttack(ThrowableObject hand, IAttackable attackable, Vector2 attackedPoint);
}
