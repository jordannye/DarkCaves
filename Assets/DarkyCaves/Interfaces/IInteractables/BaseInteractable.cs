﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseInteractable : MonoBehaviour, IInteractable
{
    public virtual float Priority { get { return 0f; } }

    /// <summary>Interface provided functionality, all IInterables will have this method</summary>
    /// <param name="playerID"></param>
    public void Interact(int playerID)
    {
        OnInteraction(playerID);
    }

    /// <summary>BaseInteractable provided functionality, this will be called directly after the interface "Interact(int)" has been invoked
    /// To stop the ability of further classes disabling behaviour</summary>
    /// <param name="playerID"></param>
    protected abstract void OnInteraction(int playerID);

    // Use this for initialization
    public virtual void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {

    }

    protected CharacterMainComponent GetPlayerMainComp(int playerID)
    {
        return PlayerController.Instance.GetPlayerObject(playerID).GetComponent<CharacterMainComponent>();
    }
}
