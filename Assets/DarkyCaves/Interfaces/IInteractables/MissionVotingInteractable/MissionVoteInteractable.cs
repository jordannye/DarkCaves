﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerDetection))]
public class MissionVoteInteractable : BaseInteractable
{
    public PlayerDetection PlayerDetection;

    public string MissionName = "";
    public string MissionSceneName = "";

    public override void Start()
    {
        base.Start();
        PlayerDetection = GetComponent<PlayerDetection>();
    }

    protected override void OnInteraction(int playerID)
    {
        Debug.Log("Player " + playerID + " has requested to interact with " + ToString() + " to start mission " + MissionName);

        if (!PhotonNetwork.player.IsMasterClient) return;
        PhotonNetwork.room.IsOpen = false;
        PlayerController.Instance.photonView.RPC("RequestLoadScene", PhotonTargets.AllBuffered, MissionSceneName);
    }
}
