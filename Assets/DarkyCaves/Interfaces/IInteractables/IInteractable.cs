﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    /// <summary>The priority of this Interactable object</summary>
    float Priority { get; }

    /// <summary>Interact with this object, identify who interacted with this object by passing in the PhotonNetwork.PlayerID</summary>
    /// <param name="playerID"></param>
    void Interact(int playerID);
}
