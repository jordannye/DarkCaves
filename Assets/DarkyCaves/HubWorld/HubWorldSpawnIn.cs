﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubWorldSpawnIn : MonoBehaviour
{
    void Start()
    {
        if (!PhotonNetwork.isMasterClient) return;
        PhotonNetwork.room.IsOpen = true;
    }
}
