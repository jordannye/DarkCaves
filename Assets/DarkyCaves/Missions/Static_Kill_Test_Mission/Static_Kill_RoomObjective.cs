﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Static_Kill_RoomObjective : RoomObjective
{
    private BadGuyController _BadGuyController;

    //private float DelayBeforeNextWaveSeconds = 2f;

    public override void Start()
    {
        base.Start();
        _BadGuyController = FindObjectOfType<BadGuyController>();
        _BadGuyController.CreateAllBadGuys();
    }

    public override Func<bool> RegisterWhenRoomIsCompleted()
    {
        return () => { return _BadGuyController.AllWavesComplete; };
    }

    protected override void InnerUpdate(float deltaTime)
    {
        if (_BadGuyController.AllWavesComplete || !_BadGuyController.CurrentWaveComplete()) return;
        _BadGuyController.SpawnNextNext();
    }

    protected override void OnRoomObjectiveComplete()
    {

    }

    protected override string GetObjectiveInformation()
    {
        if (_BadGuyController == null) _BadGuyController = FindObjectOfType<BadGuyController>();
        if (_BadGuyController.CurrentEnemies == null) return "Awaiting Objective";

        string waveInfo = string.Format("Wave: {0}/{1}", _BadGuyController.CurrentWaveIndex, _BadGuyController.BadGuys.Count());

        return string.Format("{0} Bad guys left: {1}/{2}  ", waveInfo, _BadGuyController.CurrentEnemies.Where(s => s != null && !s.BadGuyHealth.Dead).Count(), _BadGuyController.CurrentEnemies.Count());
    }
}
