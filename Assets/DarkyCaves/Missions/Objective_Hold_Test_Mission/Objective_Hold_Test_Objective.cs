﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective_Hold_Test_Objective : RoomObjective
{
    public string ObjectiveAreaName;
    private ObjectiveArea ObjectiveArea;

    /// <summary>There is no one inside the objective, if this is true</summary>
    public bool ObjectiveUnmanned = true;
    private bool _ObjectiveCompleteCalled;

    /// <summary>Seconds left inside the objective until the objective is complete</summary>
    public float TimeRemainingUntilObjectiveComplete = 30f;

    public override void Start()
    {
        base.Start();
        LinkActiveObjectiveArea();
    }

    public void LinkActiveObjectiveArea()
    {
        ObjectiveArea = GameObject.Find(ObjectiveAreaName).GetComponentInChildren<ObjectiveArea>();
    }

    public override Func<bool> RegisterWhenRoomIsCompleted()
    {
        return () => { return TimeRemainingUntilObjectiveComplete <= 0f; };
    }

    protected override void InnerUpdate(float deltaTime)
    {
        if (CheckObjectsInsideObjective(ObjectiveArea.InsideObjective))
        {
            TimeRemainingUntilObjectiveComplete -= deltaTime;
        }
    }

    protected bool CheckObjectsInsideObjective(List<GameObject> insideObjective)
    {
        bool objectiveActive = false;
        foreach (GameObject obj in insideObjective)
        {
            if (obj.GetComponentInChildren<CharacterMainComponent>() == null) continue;
            objectiveActive = true;
        }
        return objectiveActive;
    }

    protected override void OnRoomObjectiveComplete()
    {

    }

    protected override string GetObjectiveInformation()
    {
        return string.Format("Time remaining: {0}", TimeRemainingUntilObjectiveComplete.ToString("N2"));
    }
}
