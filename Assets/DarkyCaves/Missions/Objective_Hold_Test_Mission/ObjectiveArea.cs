﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class ObjectiveArea : MonoBehaviour
{
    public List<GameObject> InsideObjective;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (InsideObjective == null) InsideObjective = new List<GameObject>();

        if (collision.gameObject.GetComponentInChildren<CharacterMainComponent>() == null) return;

        if (InsideObjective.Contains(collision.gameObject)) return;
        InsideObjective.Add(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (InsideObjective == null) InsideObjective = new List<GameObject>();

        if (collision.gameObject.GetComponentInChildren<CharacterMainComponent>() == null) return;

        if (!InsideObjective.Contains(collision.gameObject)) return;
        InsideObjective.Remove(collision.gameObject);
    }
}
