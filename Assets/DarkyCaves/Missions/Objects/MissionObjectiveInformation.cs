﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>This script is assigned to a prefab that must contain this order of transform child.
/// Parent -> background box to fit behind the text
/// Only child -> This script with an attached Text component</summary>
[RequireComponent(typeof(Text))]
public class MissionObjectiveInformation : MonoBehaviour
{
    private Text _Text;
    private RectTransform _ParentBackgroundRectT;
    private RectTransform _ChildTextRectT;

    public void UpdateObjectiveInformation(string newInfo)
    {
        if (_Text == null) _Text = GetComponent<Text>();

        _Text.text = newInfo;

        Vector2 size = new Vector2(_Text.preferredWidth, _Text.preferredHeight);

        if (_ParentBackgroundRectT == null) _ParentBackgroundRectT = _Text.transform.parent.gameObject.GetComponent<RectTransform>();
        if (_ChildTextRectT == null) _ChildTextRectT = _Text.gameObject.GetComponent<RectTransform>();

        _ChildTextRectT.sizeDelta = size;
        _ParentBackgroundRectT.sizeDelta = size;
    }

}
