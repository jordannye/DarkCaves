﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomExitPortal : BaseInteractable
{
    public string NextLevelSceneName = "";

    public override float Priority { get { return 10f; } }

    protected override void OnInteraction(int playerID)
    {
        if (!PhotonNetwork.player.IsMasterClient) return;
        PlayerController.Instance.photonView.RPC("RequestLoadScene", PhotonTargets.AllBuffered, NextLevelSceneName);
    }
}
