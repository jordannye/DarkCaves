﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkyEvents;
using System;

/// <summary>A room objective needs to be complete in order to complete the last room and generate
/// the portal out of the room, and the loot chest in the room as well
/// Every RoomObjective needs a RoomExitPortal and a RoomLootChest</summary>
[RequireComponent(typeof(PhotonView))]
public abstract class RoomObjective : Photon.MonoBehaviour
{
    /// <summary>The portal in the scene that will load the next room</summary>
    private RoomExitPortal _RoomExitPortal;
    /// <summary>The loot chest that will give you the loot from this room</summary>
    private RoomLootChest _RoomLootChest;

    private MissionObjectiveInformation _MissionObjectiveInformation;

    /// <summary>If true, then this RoomObjective has been completed, spawn the Chest and Portal</summary>
    public bool Completed;
    /// <summary>The action to run, if true then it wont be called again and Completed will be marked as true</summary>
    protected Func<bool> _HasRoomCompleted;

    // Use this for initialization
    public virtual void Start()
    {
        _RoomExitPortal = FindObjectOfType<RoomExitPortal>();
        if (_RoomExitPortal == null) throw new Exception("A RoomObjective requires a RoomExitPortal in the scene otherwise the Room cannot be completed");
        _RoomExitPortal.gameObject.SetActive(false);
        _RoomLootChest = FindObjectOfType<RoomLootChest>();
        if (_RoomLootChest == null) throw new Exception("A RoomObjective requires a RoomLootChest in the scene otherwise the players do not get their Loot, which is important");
        _RoomLootChest.gameObject.SetActive(false);

        _HasRoomCompleted = RegisterWhenRoomIsCompleted();
        if (_HasRoomCompleted == null) throw new Exception("This RoomObjective has no way of knowing when it has finished, please override and add a _HasRoomCompleted action");
    }

    /// <summary>Each RoomObjective has to know when it will be complete, this Func<> will be called every 30 seconds, or upon defined events of your choice
    /// This CANNOT be null</summary>
    /// <returns></returns>
    public abstract Func<bool> RegisterWhenRoomIsCompleted();

    public void Update()
    {
        ShowObjectiveInfo(GetObjectiveInformation());
        InnerUpdate(Time.deltaTime);
        if (Completed) return;
        Completed = _HasRoomCompleted.Invoke();
        if (!Completed) return;
        photonView.RPC("RoomObjectiveComplete", PhotonTargets.AllBuffered);
    }

    private void ShowObjectiveInfo(string objectiveinformation)
    {
        if (_MissionObjectiveInformation == null) _MissionObjectiveInformation = FindObjectOfType<MissionObjectiveInformation>();
        if (_MissionObjectiveInformation == null) return;
        _MissionObjectiveInformation.UpdateObjectiveInformation(!Completed ? objectiveinformation : "Objective complete!");
    }

    /// <summary>Called as soon as the objective has been completed</summary>
    [PunRPC]
    public void RoomObjectiveComplete()
    {
        _RoomExitPortal.gameObject.SetActive(true);
        _RoomLootChest.gameObject.SetActive(true);
        OnRoomObjectiveComplete();
    }

    /// <summary>Called after the ExitPortal and the LootChest have been made visible, after the Objective being completed</summary>
    protected abstract void OnRoomObjectiveComplete();

    /// <summary>This is the update call that you can override to hook into if necessary</summary>
    /// <param name="deltaTime"></param>
    protected abstract void InnerUpdate(float deltaTime);

    /// <summary>Returns the information the player needs to know about the current objective</summary>
    /// <returns></returns>
    protected abstract string GetObjectiveInformation();

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
