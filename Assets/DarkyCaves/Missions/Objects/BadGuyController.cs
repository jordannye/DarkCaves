﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>The BadGuyController will handle the spawning of all the bad guys in this room
/// Create all of the necessary bad guys upon the creation of this script, and set inactive all of them</summary>
[RequireComponent(typeof(PhotonView))]
public abstract class BadGuyController : UpdateManger
{
    [System.Serializable]
    public class BadGuyWave
    {
        /// <summary>A prefab name for a bad guy located in the Resources/ folder</summary>
        public string ResourcePrefabName;
        /// <summary>How many are being spawned in this wave</summary>
        public int AmountInWave;
        public Rect SpawnPos;

        [System.NonSerialized]
        public List<GameObject> BadGuysInWave;
    }

    /// <summary>Resource path to prefab, and how many need to be spawned each</summary>
    public BadGuyWave[] BadGuys;

    /// <summary>The current wave of bad guys that you are on</summary>
    public int CurrentWaveIndex;

    /// <summary>The current enemies active, Alive is not the same as Active</summary>
    public List<EnemyBase> CurrentEnemies = new List<EnemyBase>();

    /// <summary>Whether you are on the last wave or not</summary>
    public bool LastWave;

    /// <summary>If you are on the last wave AND it has been completed</summary>
    public bool AllWavesComplete
    {
        get
        {
            return LastWave && CurrentWaveComplete();
        }
    }

    // Use this for initialization
    protected virtual void Start()
    {

    }

    /// <summary>Using the BadGuys array, create all of the bad guys that will be needed for this entire controller
    /// And store them, all deactivated</summary>
    public void CreateAllBadGuys()
    {
        if (!PhotonNetwork.isMasterClient) return;

        int[][] photonViewIDs = new int[BadGuys.Length][];
        Vector3[][] spawnsPos = new Vector3[BadGuys.Length][];

        for (int i = 0; i < BadGuys.Length; i++)
        {
            BadGuyWave CurrentBadGuys = BadGuys[i];

            CurrentBadGuys.BadGuysInWave = new List<GameObject>();
            string badGuyPrefab = CurrentBadGuys.ResourcePrefabName;
            int amount = CurrentBadGuys.AmountInWave;

            photonViewIDs[i] = new int[amount];
            spawnsPos[i] = new Vector3[amount];

            for (int j = 0; j < amount; j++)
            {
                GameObject soloBadGuy = PhotonNetwork.InstantiateSceneObject(badGuyPrefab, Vector3.zero, Quaternion.identity, 0, null);

                float spawnX = UnityEngine.Random.Range(CurrentBadGuys.SpawnPos.x, CurrentBadGuys.SpawnPos.x + CurrentBadGuys.SpawnPos.width);
                float spawnY = UnityEngine.Random.Range(CurrentBadGuys.SpawnPos.y, CurrentBadGuys.SpawnPos.y + CurrentBadGuys.SpawnPos.height);
                soloBadGuy.transform.parent = transform;
                soloBadGuy.transform.localPosition = new Vector3(spawnX, spawnY, soloBadGuy.transform.localPosition.z);

                spawnsPos[i][j] = soloBadGuy.transform.localPosition;
                photonViewIDs[i][j] = soloBadGuy.GetPhotonView().viewID;

                //Every bad guy has to derrive from this base class
                EnemyBase soloEnemyBase = soloBadGuy.GetComponent<EnemyBase>();
                soloEnemyBase.gameObject.SetActive(false);

                CurrentBadGuys.BadGuysInWave.Add(soloBadGuy);
            }
        }

        photonView.RPC("RegisterAllBadGuys", PhotonTargets.AllBuffered, photonViewIDs, spawnsPos);
    }

    [PunRPC]
    protected void RegisterAllBadGuys(int[][] photonViewIDs, Vector3[][] spawnpos)
    {
        for (int i = 0; i < BadGuys.Length; i++)
        {
            BadGuyWave currentBadGuy = BadGuys[i];
            currentBadGuy.BadGuysInWave = new List<GameObject>();
            int amount = currentBadGuy.AmountInWave;

            for (int j = 0; j < amount; j++)
            {
                GameObject soloBadGuy = PhotonView.Find(photonViewIDs[i][j]).gameObject;
                soloBadGuy.transform.parent = transform;
                soloBadGuy.transform.localPosition = spawnpos[i][j];

                soloBadGuy.gameObject.SetActive(false);

                currentBadGuy.BadGuysInWave.Add(soloBadGuy);
            }
        }
    }

    /// <summary>Spawn the next set of bad guys</summary>
    public void SpawnNextNext()
    {
        if (!PhotonNetwork.isMasterClient) return;

        CurrentEnemies = new List<EnemyBase>();
        BadGuyWave currentBadGuyWave = BadGuys[CurrentWaveIndex];
        for (int i = 0; i < currentBadGuyWave.BadGuysInWave.Count; i++)
        {
            GameObject badGuyObject = currentBadGuyWave.BadGuysInWave[i];
            badGuyObject.SetActive(true);
            CurrentEnemies.Add(badGuyObject.GetComponent<EnemyBase>());
        }
        CurrentWaveIndex++;
        LastWave = CurrentWaveIndex >= BadGuys.Length;

        int[] photonViewIDs = new int[CurrentEnemies.Count];
        Vector3[] spawnPos = new Vector3[CurrentEnemies.Count];

        for (int i = 0; i < CurrentEnemies.Count; i++)
        {
            EnemyBase curBadGuy = CurrentEnemies[i];
            photonViewIDs[i] = curBadGuy.GetComponent<PhotonView>().viewID;
            spawnPos[i] = curBadGuy.transform.localPosition;
        }

        photonView.RPC("SaveCurrentBadGuys", PhotonTargets.AllBuffered, photonViewIDs, spawnPos);
    }

    /// <summary>Save the CurrentBadGuys, this is called by the Master to all clients to sync up the CurrentBadGuys</summary>
    /// <param name="photonViewIDs"></param>
    [PunRPC]
    protected void SaveCurrentBadGuys(int[] photonViewIDs, Vector3[] badGuysSpawnPos)
    {
        CurrentEnemies = new List<EnemyBase>();
        for (int i = 0; i < photonViewIDs.Length; i++)
        {
            GameObject gObj = PhotonView.Find(photonViewIDs[i]).gameObject;
            gObj.SetActive(true);
            gObj.transform.localPosition = badGuysSpawnPos[i];
            CurrentEnemies.Add(gObj.GetComponent<EnemyBase>());
        }
    }

    public bool CurrentWaveComplete()
    {
        //Only the master can determine the end of the room
        if (!PhotonNetwork.isMasterClient) return false;
        //If there are any enemies not dead, then the wave isn't complete
        return CurrentEnemies != null && CurrentEnemies.Where(s => s != null && !s.BadGuyHealth.Dead).Count() == 0;
    }



    /** ----------------------- NETWORKING STUFF ------------------------- **/

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
