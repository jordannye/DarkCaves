﻿using Assets.New_DarkyCaves.EventManager.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DarkyEvents
{
    public enum EventNames
    {
        //ABILITY EVENTS
        /// <summary>A passive has been equipped to the player</summary>
        PassiveAdded,
        /// <summary>A passive has been activated</summary>
        PassiveActivated,

        /// <summary>Event fired when the player blinks</summary>
        BlinkTriggered,
        /// <summary>Event fired when the player's blink has cooled down and is ready to us</summary>
        BlinkReady,

        //CHARACTER EVENTS

        HealthFirstInitialised,

        CharacterDamaged,

        RechargeShieldStart,
        CharacterShieldDepleted,

        //HUB EVENTS
        /// <summary>The player has updated their abilities in the HUB</summary>
        PlayerAbilitiesUpdated,

        /// <summary>The HUB has been paused with the menu being open</summary>
        HubPauseMenu
    }

    public class EventManager : MonoBehaviour
    {
        private Dictionary<EventNames, UnityEvent<System.Object>> _EventDictionary;

        private static EventManager _EventManager;
        public static EventManager Instance
        {
            get
            {
                if (_EventManager == null)
                {
                    _EventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                    if (_EventManager == null) Debug.LogError("There needs to be an EventManager somewhere in the scene");
                    else
                    {
                        _EventManager.Init();
                    }
                }
                return _EventManager;
            }
        }

        private void Init()
        {
            if (_EventDictionary == null)
            {
                _EventDictionary = new Dictionary<EventNames, UnityEvent<System.Object>>();
            }
        }

        /// <summary>Add a listener to the eventName
        /// If the event name hasn't been stored for listenering previously, then it will create the list of events</summary>
        /// <param name="eventName"></param>
        /// <param name="listener"></param>
        public static void StartListening(EventNames eventName, UnityAction<System.Object> listener)
        {
            UnityEvent<System.Object> thisEvent = null;
            if (Instance._EventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityObjectEvent();
                thisEvent.AddListener(listener);
                Instance._EventDictionary.Add(eventName, thisEvent);
            }
        }

        /// <summary>Request that this listener is removed from listening to events with the certain eventnames</summary>
        /// <param name="eventName"></param>
        /// <param name="listener"></param>
        public static void StopListening(EventNames eventName, UnityAction<System.Object> listener)
        {
            if (_EventManager == null) return;
            UnityEvent<System.Object> thisEvent = null;
            if (!Instance._EventDictionary.TryGetValue(eventName, out thisEvent)) return;
            thisEvent.RemoveListener(listener);
        }

        /// <summary>You can only trigger an event if the object you are triggering it from is yours
        /// In Photon that means if the PhotonView.isMine is true</summary>
        /// <param name="eventName">The name of the event</param>
        /// <param name="eventObj">Data you want to send through the event</param>
        /// <param name="isMine">If false and this IS a network event, then nothing will happen</param>
        /// <param name="networkedEvent">If this is a networked event, meaning if this is being called on all objects across the scene after being called via NPC on the network</param>
        public static void TriggerEvent(EventNames eventName, System.Object eventObj, bool isMine, bool networkedEvent = true)
        {
            if (!isMine && networkedEvent) return;

            UnityEvent<System.Object> thisEvent = null;
            if (!Instance._EventDictionary.TryGetValue(eventName, out thisEvent)) return;
            thisEvent.Invoke(eventObj);
        }
    }
}