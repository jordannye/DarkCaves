﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Assets.New_DarkyCaves.EventManager.Events
{
    [System.Serializable]
    class UnityObjectEvent : UnityEvent<System.Object>
    {

    }
}
