﻿using DarkyEvents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(CharacterAttackable))]
public class CharacterStats : BaseStats
{
    /// <summary>The rigidbody of this character</summary>
    private Rigidbody2D CharacterRigidBody2D;
    private CharacterAttackable _CharacterAttackable;

    public override void Start()
    {
        CharacterRigidBody2D = GetComponent<Rigidbody2D>();
        _CharacterAttackable = GetComponent<CharacterAttackable>();

        CalculateBaseStats();

        DarkyEvents.EventManager.StartListening(EventNames.PlayerAbilitiesUpdated, CalculateBaseStats);
    }

    /// <summary>Calcualte the CharacterStats using the PlayerPref storage, otherwise default each BaseState to a default value</summary>
    /// <param name="obj"></param>
    private void CalculateBaseStats(System.Object obj = null)
    {
        PhotonView pv = GetComponent<PhotonView>();
        if (!pv.isMine) return;

        BaseShield = (float)PlayerPrefs.GetInt("Shield");

        BaseHealth = (float)PlayerPrefs.GetInt("Health");

        BaseDamage = (float)PlayerPrefs.GetInt("Damage");

        pv.RPC("UpdateStats", PhotonTargets.All, BaseShield, BaseHealth, BaseDamage);
    }

    [PunRPC]
    private void UpdateStats(float baseShield, float baseHealth, float baseDamage)
    {
        BaseShield = baseShield;
        BaseHealth = baseHealth;
        BaseDamage = baseDamage;

        //The throw speed is calculated by the BaseDamage value. 
        BaseHandThrownSpeedMultiplier = ConvertDamageToThrowSpeed(BaseDamage);

        // D = S x T.. but capped to 8
        BaseHandThrowDistance = ConvertDamageToThrowDistance(BaseDamage);

        //Calculate the returning speed of the hands, not based on the throw speed but on the smooth follow maths. The conversion rate is about 0.0125f
        BaseIdleHandDamperening = BaseMovementSpeed * 0.001f;

        //400 hp = 50%
        CCResistance = ConvertHealthToCCRes(BaseHealth);

        if (_CharacterAttackable == null || _CharacterAttackable.HealthType == null) return;

        _CharacterAttackable.HealthType.Health = BaseHealth;
        _CharacterAttackable.HealthType.Shield = BaseShield;

        _CharacterAttackable.HealthType.RegisterHealthType(this);

        DarkyEvents.EventManager.TriggerEvent(EventNames.HealthFirstInitialised, _CharacterAttackable.HealthType, gameObject.IsMine());
    }

    /// <summary>Convert the BaseDamage from the Players Stats into the additional ThrowDistance for the players fists</summary>
    /// <param name="baseDamage"></param>
    /// <returns></returns>
    public static float ConvertDamageToThrowDistance(float baseDamage)
    {
        return Math.Min(ConvertDamageToThrowSpeed(baseDamage) * BaseHandTimeOut, 8f);
    }

    /// <summary>Convert the BaseDamage into the Throw speed for the Players fists</summary>
    /// <param name="baseDamage"></param>
    /// <returns></returns>
    public static float ConvertDamageToThrowSpeed(float baseDamage)
    {
        return (float)diminishing_returns(baseDamage, 2.5/4);
    }

    /// <summary>Convert the players BaseHealth into the CC resistance state generated by the extra health</summary>
    /// <param name="health"></param>
    /// <returns></returns>
    public static float ConvertHealthToCCRes(float health)
    {
        return (float)diminishing_returns(health, 0.7/4);
    }

    /// <summary>Convert the BaseShield from the player into extra Movement speed for the player</summary>
    /// <param name="shield"></param>
    /// <returns></returns>
    public static float ConvertShieldToMS(float shield)
    {
        return (float)diminishing_returns(shield, 5.76/4);
    }

    /// <summary>Return the current health of the player</summary>
    /// <returns></returns>
    public override float CurrentHealth()
    {
        return _CharacterAttackable.HealthType.Health;
    }

    /// <summary>Return the current Shield of the player</summary>
    /// <returns></returns>
    public override float CurrentShield()
    {
        return _CharacterAttackable.HealthType.Shield;
    }

    /// <summary>The movement speed for this unit</summary>
    /// <returns></returns>
    protected override float CalculateExtraMovementSpeed()
    {
        return ConvertShieldToMS(BaseShield);
    }

    public override void IUpdate(float deltaTime)
    {

    }

    //////////////// --------------------------------------- ///////////////////////////////
    /// THROWING STATS
    /// 

    /// <summary>The multiplier to decrease/increase the speed of a thrown fist</summary>
    public float BaseHandThrownSpeedMultiplier = 40f;

    /// <summary>The distance the hand will be targetting to travel to, can travel past this point but this is the point at which both hands will target
    /// along the ThrowVector from the Character's body</summary>
    public float BaseHandThrowDistance = 10f;

    /// <summary>The base amount of seconds a hand is allowed to be in the OUT state before RETURNING</summary>
    public static float BaseHandTimeOut = 0.3f;
    /// <summary>The base amount of seconds a hand HAS to be OUT before it is allowed to be RETURNING</summary>
    public float BaseHandTimeTilCanReturn = 0.1f;

    /// <summary>The value to keep the hand close to the characters arm anchor, small to keep it very tight</summary>
    public float BaseIdleHandDamperening = 1f;

    public float BaseJumpSpeed = 100f;

    /// <summary>Calculate the speed multiplier at which the thrown hand can travel with</summary>
    /// <returns></returns>
    public float CalculateHandThrownSpeedMultiplier()
    {
        return BaseHandThrownSpeedMultiplier;
    }

    private void OnDestroy()
    {
        DarkyEvents.EventManager.StopListening(EventNames.PlayerAbilitiesUpdated, CalculateBaseStats);
    }
}
