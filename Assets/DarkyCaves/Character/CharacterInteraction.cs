﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(CharacterMainComponent))]
public class CharacterInteraction : MonoBehaviour, IUpdateable
{
    /// <summary>If the Character is allowed to interact, you have a delay between being able to interact</summary>
    public bool Interactable;
    /// <summary>Delay between being able to interact</summary>
    private float _InteractDelay = 0.3f;
    /// <summary>Current time delay before you are allowed to interact again</summary>
    private float _CurrentTimeSinceLastInteracted = 0f;

    private CharacterMainComponent _CharacterMainComp;
    private CharacterControllerSwitcher _CharacterControllerSwitcher;

    public IInteractable _CurrentInteractable;

    // Use this for initialization
    void Start()
    {
        _CharacterMainComp = GetComponent<CharacterMainComponent>();
        _CharacterControllerSwitcher = GetComponent<CharacterControllerSwitcher>();
    }

    public void IUpdate(float deltaTime)
    {
        if (!Interactable)
        {
            _CurrentTimeSinceLastInteracted -= deltaTime;
            Interactable = _CurrentTimeSinceLastInteracted <= 0f;
            if (!Interactable) return;
        }

        if (_CharacterControllerSwitcher == null) _CharacterControllerSwitcher = GetComponent<CharacterControllerSwitcher>();
        if (_CharacterControllerSwitcher == null) return;
        if (Interactable && !_CharacterControllerSwitcher.CheckInteractionButtonPressed()) return;
        Interactable = false;
        _CurrentTimeSinceLastInteracted = _InteractDelay;
        TriggerInteraction();
    }

    protected void TriggerInteraction()
    {
        if (_CurrentInteractable == null) return;
        _CurrentInteractable.Interact(PhotonNetwork.player.ID);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_CharacterMainComp.photonView.isMine) return;

        List<IInteractable> interactable = ((List<IInteractable>)collision.gameObject.GetAllAttachedTypes(typeof(IInteractable), true));
        if (interactable == null || interactable.Count <= 0) return;
        interactable.Sort((a, b) => a.Priority.CompareTo(b.Priority));
        _CurrentInteractable = interactable.First();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!_CharacterMainComp.photonView.isMine) return;

        List<IInteractable> interactable = ((List<IInteractable>)collision.gameObject.GetAllAttachedTypes(typeof(IInteractable), true));
        if (interactable == null || interactable.Count <= 0 && !interactable.Contains(_CurrentInteractable)) return;
        _CurrentInteractable = null;
    }
}
