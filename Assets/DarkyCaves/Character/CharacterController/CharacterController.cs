﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterController
{
    public class CharacterInput
    {
        public static float StickMinThreshold = 0.05f;

        public static string LeftStickX = "LeftJoystickX";
        public static string LeftStickY = "LeftJoystickY";

        public static string RightStickX = "RightJoystickX";
        public static string RightStickY = "RightJoystickY";

        public static string LeftTrigger = "LeftTrigger";
        public static float LeftTriggerThreshHold = 0.25f;
        public static string RightTrigger = "RightTrigger";
        public static float RightTriggerThreshHold = 0.25f;

        public static string ThrowLeft = "ThrowLeft";
        public static string ThrowRight = "ThrowRight";

        public static string Start = "Escape";
        public static string Action = "Action";
        public static string X = "X";
        public static string Blink = "Blink";

        public static string MoveForward = "MoveForward";
        public static string MoveBackwards = "MoveBackwards";
        public static string MoveLeft = "MoveLeft";
        public static string MoveRight = "MoveRight";

        public static string LeftMouse = "LeftMouse";
        public static string RightMouse = "RightMouse";
        public static string LeftShift = "LeftShift";

        public static string SpaceBar = "SpaceBar";
    }

    public bool Active { get; set; }

    /// <summary>This is the Transform object that is being considered the Character for this controller to manipulate</summary>
    public CharacterMainComponent _CharacterTranform;
    private Rigidbody2D _CharacterRigidBody;

    public CharacterControllerSwitcher.CharacterControllerTypes CharacterControllerType { get; private set; }


    public enum DirectionalState
    {
        IDLE,
        UP, DOWN,
        LEFT, RIGHT
    }
    /// <summary>The direction at which the character is looking</summary>
    public DirectionalState CharacterDirection = DirectionalState.DOWN;

    /// <summary>The state that the character can be in, whee a force is being applied to them. eg, jumping</summary>
    public enum ForceStates
    {
        GROUNDED,
        JUMPING,
        FALLING
    }

    public ForceStates CurrentForceState = ForceStates.FALLING;

    /// <summary>The time where it applies the jump force to the character, after this time no more jump force is applied</summary>
    protected float _TimeForApplyJumpPressure = 0.2f;
    /// <summary>Once a jump has occured, after you have grounded, wait this time before you can jump again</summary>
    protected float _TimeBetweenJumps = 0.1f;

    /// <summary>The time left before jump force is ignored</summary>
    protected float _CurrentApplyJumpPressure = 0f;
    /// <summary>The time left before you detect jumping again</summary>
    protected float _CurrentWaitBeforeJumpAllowed;
    protected bool AllowedToJump { get { return _CurrentWaitBeforeJumpAllowed <= 0 && CurrentForceState == ForceStates.JUMPING; } }

    // -----------------------

    public CharacterController(CharacterControllerSwitcher.CharacterControllerTypes controllerType, CharacterMainComponent characterTransform, LineRenderer throwDirection)
    {
        _CharacterTranform = characterTransform;
        _CharacterRigidBody = characterTransform.GetComponent<Rigidbody2D>();
        _ThrowDirectionLine = throwDirection;
        CharacterControllerType = controllerType;
    }

    // Use this for initialization
    public virtual void Start()
    {

    }

    /// <summary>Will be called in the event at which the character is moving in a direction that is different than the current facing vector</summary>
    /// <param name="newDirection"></param>
    public void CharacterDirectionalChange(DirectionalState newDirection)
    {
        if (CharacterDirection == newDirection) return;
        CharacterDirection = newDirection;
        _CharacterTranform.photonView.RPC("UpdateDirectionalState", PhotonTargets.All, newDirection);
    }

    private Vector3 _PreviousMoveVec;

    // Update is called once per frame
    public virtual void FixedUpdate(float deltaTime)
    {
        Vector3 normFaceVec = _CharacterTranform.FacingVector.normalized;
        Vector3 movementVec = CalculateMovementVector();
        MoveCharacter(movementVec);
        Vector3 normMoveVec = movementVec.normalized;
        _PreviousMoveVec = normMoveVec;

        float rawXMove = CalculateMovementVector().x;
        float xMove = Mathf.Abs(rawXMove);

        if (xMove > 0)
        {
            if (rawXMove > 0) CharacterDirectionalChange(DirectionalState.RIGHT);
            else if (rawXMove < 0) CharacterDirectionalChange(DirectionalState.LEFT);
        }

        //else if (normFaceVec.y < 0 && normMoveVec.x > 0) CharacterDirectionalChange(DirectionalState.UP);
        //else if (normFaceVec.y > 0 && normMoveVec.x < 0) CharacterDirectionalChange(DirectionalState.DOWN);

        else CharacterDirectionalChange(DirectionalState.IDLE);
    }

    public virtual void Update(float deltaTime)
    {
        Vector3 throwVector = CalculateThrowVector();
        DisplayThrowDirection(throwVector);
        //If you have a direction to throw something, check to see if you want to throw something
        if (throwVector != Vector3.zero)
        {

            if (CheckToThrowLeftHand())
            {
                TriggerThrowObject(throwVector);
            }
            if (CheckToThrowRightHand())
            {
                TriggerThrowObject(throwVector);
            }
        }

        if (CheckWantToBlink())
        {
            Vector3 blinkVector = throwVector == Vector3.zero ? _CharacterTranform.FacingVector.normalized : throwVector;
            _CharacterTranform.photonView.RPC("RequestTriggerBlink", PhotonTargets.MasterClient, new object[] { blinkVector.normalized });
        }


        switch (CurrentForceState)
        {
            case ForceStates.GROUNDED:
                _CurrentWaitBeforeJumpAllowed -= Time.deltaTime;
                if (_CurrentWaitBeforeJumpAllowed < 0 && CheckForJump())
                {
                    ChangeForceState(ForceStates.JUMPING);
                    _CurrentApplyJumpPressure = _TimeForApplyJumpPressure;
                }
                break;
            case ForceStates.JUMPING:
                _CurrentApplyJumpPressure -= Time.deltaTime;
                if (_CurrentApplyJumpPressure < 0) ChangeForceState(ForceStates.FALLING);
                break;
            case ForceStates.FALLING:
                if (DetectGround())
                {
                    ChangeForceState(ForceStates.GROUNDED);
                    _CurrentWaitBeforeJumpAllowed = _TimeBetweenJumps;
                }
                break;
        }

        if (AllowedToJump)
        {
            if (CheckForJump()) ApplyJumpForce();
            else ChangeForceState(ForceStates.FALLING);
        }
    }

    protected void ApplyJumpForce()
    {
        _CharacterTranform.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1 * _CharacterTranform.CharacterStats.BaseJumpSpeed));
    }

    protected bool DetectGround()
    {
        var result = _CharacterRigidBody.IsTouchingLayers(LayerMask.GetMask("Floor"));
        return result;
    }

    protected void ChangeForceState(ForceStates newState)
    {
        if (CurrentForceState == newState) return;
        CurrentForceState = newState;
        _CharacterTranform.photonView.RPC("UpdateForceState", PhotonTargets.All, newState);
    }

    /// <summary>Calculate and return the Normalised Vector direction you want the character to attempt to travel in</summary>
    /// <returns></returns>
    protected abstract Vector3 CalculateMovementVector();

    /// <summary>Move the character after asking for the vector you wish to move the character in</summary>
    /// <param name="nMovementVector"></param>
    private void MoveCharacter(Vector3 nMovementVector)
    {
        _CharacterTranform.MoveCharacterAlongVector(nMovementVector);
    }

    /// <summary>Component used for displaying the Throw vector</summary>
    protected LineRenderer _ThrowDirectionLine;

    /// <summary>Calculate the Vector out from the centre of the Character, in the direction you wish to Throw</summary>
    /// <returns></returns>
    protected abstract Vector3 CalculateThrowVector();

    /// <summary>Show the Vector that you are aming your Throw in</summary>
    /// <param name="throwVector"></param>
    protected virtual void DisplayThrowDirection(Vector3 throwVector)
    {
        //if (throwVector.magnitude < 0.5f) return;
        Vector3 startPos = _CharacterTranform.transform.position;
        _ThrowDirectionLine.SetPosition(0, startPos);
        _ThrowDirectionLine.SetPosition(1, startPos + throwVector);
    }

    /// <summary>Perform a check to see if this controller is in use, if this returns true then this will be the only controller set to active</summary>
    /// <returns></returns>
    public abstract bool CheckActivelyUsed();

    /// <summary>The current time left before you can throw your left hand again</summary>
    protected TimeSpan _LeftHandThrowCooldown;
    /// <summary>The current time left before you can throw your right hand again</summary>
    protected TimeSpan _RightHandThrowCooldown;

    /// <summary>Whether or not the LeftHand has returned and is in position to be fired again</summary>
    public bool LeftHandInPosition
    {
        get { return _CharacterTranform.ThrowableObject.CurrentThrownState == ThrowableObject.ThrowableState.IDLE; }
    }
    /// <summary>Whether or not the RightHand has returned and is in position to be fired again</summary>
    public bool RightHanInPosition
    {
        get { return _CharacterTranform.ThrowableObject.CurrentThrownState == ThrowableObject.ThrowableState.IDLE; }
    }

    /// <summary>Check to see if you want to throw the left hand
    /// This requires a ThrowVector to be decided before returning true to have an effect</summary>
    /// <returns></returns>
    protected abstract bool CheckToThrowLeftHand();

    /// <summary>Check to see if you want to throw the right hand
    /// This requires a ThrowVector to be decided before returning true to have an effect</summary>
    /// <returns></returns>
    protected abstract bool CheckToThrowRightHand();

    /// <summary>Initial a throw of a hand</summary>
    /// <param name="index"> 0 left</param>
    protected void TriggerThrowObject(Vector3 throwVector)
    {
        ThrowableObject hand = _CharacterTranform.ThrowableObject;
        switch (hand.CurrentThrownState)
        {
            case ThrowableObject.ThrowableState.IDLE:
                _CharacterTranform.photonView.RPC("RequestThrowObject", PhotonTargets.MasterClient, new object[] { _CharacterTranform.CalculateHandThrowVector(throwVector) });
                break;
            case ThrowableObject.ThrowableState.OUT:
                _CharacterTranform.photonView.RPC("RequestUserEarlyStartReturning", PhotonTargets.MasterClient);
                break;
            case ThrowableObject.ThrowableState.RETURNING:
                break;
        }
    }


    /// <summary>Check if the user wants to blink</summary>
    /// <returns></returns>
    protected abstract bool CheckWantToBlink();

    /// <summary>The player is trying to interact with something near them</summary>
    /// <returns></returns>
    public abstract bool CheckWantToInteract();

    /// <summary>Check the jump action has been asked for</summary>
    /// <returns></returns>
    public abstract bool CheckForJump();
}
