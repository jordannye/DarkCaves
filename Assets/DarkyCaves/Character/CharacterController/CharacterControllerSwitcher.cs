﻿using DarkyEvents;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The purpose of this class is to allow for multiple CharacterControllers to be in the game, but this will only allow one to be acting on the character at
/// one time.
/// 
/// Example;
/// GamepadCharacterController and KeyboardMouseController are both added.
/// Upon the ACTION button being pressed by either source, this class will be checking for this, it will activate that CharacterController and disable all of the others.</summary>
[RequireComponent(typeof(CharacterMainComponent), typeof(LineRenderer))]
public class CharacterControllerSwitcher : MonoBehaviour
{
    public bool PauseUpdate;
    private void SetPauseUpdate(System.Object pause) { PauseUpdate = (bool)pause; }

    /// <summary>These are the action the character can perform, use these to grab check for the current behaviour 
    /// from the current controller scheme without having to know the actual setup</summary>
    public enum ButtonKeys
    {
        THROW_LEFT,
        THROW_RIGHT,
        BLINK,
        INTERACT
    }

    public enum CharacterControllerTypes
    {
        Gamepad,
        KeyboardMouse
    }
    public CharacterControllerTypes CurrentActiveControllerType = CharacterControllerTypes.KeyboardMouse;
    public List<CharacterControllerTypes> InstallCharacterControllers;

    public CharacterController CurrentCharacterController { get { return _AvailableCharacterControllers.FirstOrDefault(s => s.Active); } }

    private LineRenderer _LineRenderer;

    private CharacterController CreateController(CharacterControllerTypes controllerType)
    {
        switch (controllerType)
        {
            case CharacterControllerTypes.Gamepad:
                return new GamepadCharacterController(controllerType, GetComponent<CharacterMainComponent>(), _LineRenderer);
            case CharacterControllerTypes.KeyboardMouse:
                return new KeyboardMouseCharacterController(controllerType, GetComponent<CharacterMainComponent>(), _LineRenderer);
        }
        return null;
    }

    private List<CharacterController> _AvailableCharacterControllers = new List<CharacterController>();

    // Use this for initialization
    void Start()
    {
        if (InstallCharacterControllers == null) return;
        _LineRenderer = GetComponent<LineRenderer>();
        _LineRenderer.sortingLayerName = GetComponentInChildren<SpriteRenderer>().sortingLayerName;

        for (int i = 0; i < InstallCharacterControllers.Count; i++)
        {
            CharacterControllerTypes controllerType = InstallCharacterControllers[i];
            CharacterController cc = CreateController(controllerType);
            if (cc == null) continue;
            cc.Active = i == 0;
            _AvailableCharacterControllers.Add(cc);
        }

        DarkyEvents.EventManager.StartListening(EventNames.HubPauseMenu, this.SetPauseUpdate);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (PauseUpdate) return;

        foreach (CharacterController cc in _AvailableCharacterControllers)
        {
            if (cc.CheckActivelyUsed()) ActivateDifferentController(cc.CharacterControllerType);
            if (!cc.Active) continue;
            CurrentActiveControllerType = cc.CharacterControllerType;
            cc.FixedUpdate(Time.deltaTime);
        }
    }

    void Update()
    {
        if (PauseUpdate) return;

        foreach (CharacterController cc in _AvailableCharacterControllers)
        {
            if (!cc.Active) continue;
            cc.Update(Time.deltaTime);
        }
    }

    /// <summary>Called when there has been a detection of a different type of controller attempting to be used that isn't active.
    /// Swap over to use that controller instead!</summary>
    /// <param name="newControllerType"></param>
    protected void ActivateDifferentController(CharacterControllerTypes newControllerType)
    {
        foreach (CharacterController cc in _AvailableCharacterControllers) cc.Active = cc.CharacterControllerType == newControllerType;
    }

    public bool CheckInteractionButtonPressed()
    {
        return !PauseUpdate && CurrentCharacterController.CheckWantToInteract();
    }

    private void OnDestroy()
    {
        DarkyEvents.EventManager.StopListening(EventNames.HubPauseMenu, this.SetPauseUpdate);
    }
}
