﻿using DarkyEvents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>To apply a buff to a character, either that of a Passive, OnBlink or ThrowableItem
/// You have to use this controller</summary>
[RequireComponent(typeof(CharacterMainComponent), typeof(CharacterStats))]
public class CharacterBuffController : MonoBehaviour, IUpdateable
{
    private CharacterMainComponent _CharacterMain;
    private CharacterStats _CharacterStats;

    /// <summary>The currently equipped passives</summary>
    public IPassive[] EquippedPassives;

    /// <summary>The current index for which passive in the list is active</summary>
    public int ActivePassiveIndex;

    private void Start()
    {
        DarkyEvents.EventManager.StartListening(EventNames.PlayerAbilitiesUpdated, this.PlayerAbilitiesUpdate);
    }

    private void PlayerAbilitiesUpdate(System.Object passives)
    {
        string[] passiveNames = (string[])passives;
        EquippedPassives = null;
        ActivePassiveIndex = 0;
        LoadPassives();
        DarkyEvents.EventManager.TriggerEvent(EventNames.PassiveActivated, ActivePassiveIndex, _CharacterMain.photonView.isMine);
    }

    /// <summary>Load the passives you have equipped to be on your character</summary>
    public void LoadPassives()
    {
        _CharacterMain = GetComponent<CharacterMainComponent>();
        _CharacterStats = GetComponent<CharacterStats>();

        //Have to ignore the disabled components here, because if you return from the menu just after updating the player's components, the old ones can still be attached while being destroyed
        //But they are disabled, so checking for that will ignore them
        List<IPassive> wantedPassives = ((List<IPassive>)gameObject.GetAllAttachedTypes(typeof(IPassive), true));

        EquippedPassives = new IPassive[wantedPassives.Count];

        //Add all the 
        for (int i = 0; i < EquippedPassives.Length; i++)
        {
            IPassive addMe = null;
            if (wantedPassives.Count > 0)
            {
                addMe = wantedPassives[0];
                wantedPassives.RemoveAt(0);
            }
            else addMe = _CharacterMain.gameObject.AddComponent<DefaultWaitPassive>();
            EquippedPassives[i] = addMe;
            ((MonoBehaviour)EquippedPassives[i]).enabled = false;
        }
        ActivePassiveIndex = 0;
        ((MonoBehaviour)EquippedPassives[ActivePassiveIndex]).enabled = true;

        while (wantedPassives.Count > 0)
        {
            Destroy((MonoBehaviour)wantedPassives[0]);
            wantedPassives.RemoveAt(0);
        }

        for (int i = 0; i < EquippedPassives.Length; i++)
        {
            EquipPassive(i, EquippedPassives[i]);
        }
    }

    private void EquipPassive(int index, IPassive passive)
    {
        //if (EquippedPassives[index] != null) Debug.LogError("You are trying to equip a passive in a slot that is already taken by a passive");
        EquippedPassives[index] = passive;

        if (!_CharacterMain.photonView.isMine) return;
        DarkyEvents.EventManager.TriggerEvent(EventNames.PassiveAdded, new { Index = index, Passive = passive }, _CharacterMain.photonView.isMine);
    }

    public void IUpdate(float deltaTime)
    {
        EquippedPassives[ActivePassiveIndex].IUpdate(deltaTime);
        if (!EquippedPassives[ActivePassiveIndex].PassiveCompleted()) return;
        CurrentPassiveDeactivated();
        IncrementPassiveIndex();
        CurrentPassiveActivated();
    }

    private void IncrementPassiveIndex()
    {
        ActivePassiveIndex++;
        if (ActivePassiveIndex >= EquippedPassives.Length) ActivePassiveIndex = 0;

        DarkyEvents.EventManager.TriggerEvent(EventNames.PassiveActivated, ActivePassiveIndex, _CharacterMain.photonView.isMine);
    }

    public void CurrentPassiveDeactivated()
    {
        EquippedPassives[ActivePassiveIndex].OnPassiveDeactivate(_CharacterMain);
        ((MonoBehaviour)EquippedPassives[ActivePassiveIndex]).enabled = false;
    }

    public void CurrentPassiveActivated()
    {
        ((MonoBehaviour)EquippedPassives[ActivePassiveIndex]).enabled = true;
        EquippedPassives[ActivePassiveIndex].OnPassiveActivate(_CharacterMain);
    }

    private void OnDestroy()
    {
        DarkyEvents.EventManager.StopListening(EventNames.PlayerAbilitiesUpdated, this.PlayerAbilitiesUpdate);
    }
}
