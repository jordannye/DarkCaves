﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    public SpriteRenderer CharacterSpriteRenderer;

    public Animator Animator;

    private CharacterController.DirectionalState _DirectionalState;
    private CharacterController.ForceStates _ForceState;

    private Rigidbody2D _RigidBody2D;
    private Rigidbody2D RigidBody2D
    {
        get
        {
            if (_RigidBody2D == null) _RigidBody2D = GetComponentInParent<Rigidbody2D>();
            return _RigidBody2D;
        }
    }

    // Use this for initialization
    void Start()
    {
        Animator = GetComponent<Animator>();
    }

    public void UpdateDirectionalState(CharacterController.DirectionalState newDirection)
    {
        _DirectionalState = newDirection;
        switch (newDirection)
        {
            case CharacterController.DirectionalState.IDLE:
                Animator.SetBool("Walking", false);
                break;
            case CharacterController.DirectionalState.LEFT:
            case CharacterController.DirectionalState.RIGHT:
                Animator.SetBool("Walking", true);
                break;
        }
    }

    public void UpdateForceState(CharacterController.ForceStates newForce)
    {
        switch (newForce)
        {
            case CharacterController.ForceStates.JUMPING:
            case CharacterController.ForceStates.FALLING:
                Animator.SetBool("Jumping", true);
                break;
            case CharacterController.ForceStates.GROUNDED:
                Animator.SetBool("Jumping", false);
                break;
        }
        _ForceState = newForce;
    }
}
