﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SmoothFollow))]
public class FindTargetToSmoothFollow : MonoBehaviour
{
    private SmoothFollow _SmoothFollow;
    public Transform TransformToFollow = null;

    // Use this for initialization
    void Start()
    {
        _SmoothFollow = GetComponent<SmoothFollow>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_SmoothFollow == null || _SmoothFollow.target != null) Destroy(this);
        if (TransformToFollow == null) return;
        _SmoothFollow.target = TransformToFollow.transform;
    }
}
