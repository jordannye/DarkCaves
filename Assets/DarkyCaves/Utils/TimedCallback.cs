﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Objects.Utility
{
    public class TimedCallback : MonoBehaviour
    {
        public void Begin(float seconds, Action callback)
        {
            StartCoroutine(WaitAfterSeconds(seconds, callback));
        }


        private IEnumerator WaitAfterSeconds(float seconds, Action callback)
        {
            yield return new WaitForSeconds(seconds);
            if (callback != null) callback.Invoke();
            callback = null;
            Destroy(this);
        }
    }
}
