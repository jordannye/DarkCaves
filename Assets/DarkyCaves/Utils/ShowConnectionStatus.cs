﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowConnectionStatus : Photon.PunBehaviour
{
    private void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }
}
