﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D), typeof(PointEffector2D))]
public class ApplyDamageInCircleCollider : MonoBehaviour
{
    protected CircleCollider2D _ExplosionRadius;
    protected float _EndRadius;

    protected PointEffector2D _PointEffector;

    protected int _OwnerPlayerID;

    protected DamageType _Damage;

    public List<IAttackable> _AttackedObjects = new List<IAttackable>();

    // Use this for initialization
    void Start()
    {
        if (_ExplosionRadius != null) return;
        LinkComponents();
    }

    private void LinkComponents()
    {
        _ExplosionRadius = GetComponent<CircleCollider2D>();
        _EndRadius = _ExplosionRadius.radius;
        _ExplosionRadius.radius = 0f;

        _PointEffector = GetComponent<PointEffector2D>();
    }

    public void RegisterAndExplode(int playerID, DamageType damage)
    {
        if (_ExplosionRadius == null) LinkComponents();

        //If the playerID != -1, then this is a player created weapon. Otherwise its an enemy created weapon.
        //TODO Have a global ID system to allow for enemies to also have ID's
        int thisLayer = playerID != -1 ? LayerMask.NameToLayer("PlayerWeapons") : LayerMask.NameToLayer("EnemyWeapons");
        _ExplosionRadius.gameObject.layer = thisLayer;

        string layerMask = playerID != -1 ? "Enemy" : "Player";
        _PointEffector.colliderMask = LayerMask.GetMask(layerMask);

        _OwnerPlayerID = playerID;
        _Damage = damage;
        _ExplosionRadius.radius = _EndRadius;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        List<IAttackable> attackables = (List<IAttackable>)collision.gameObject.GetAllAttachedTypes(typeof(IAttackable), true);
        if (attackables == null) return;
        foreach (IAttackable attk in attackables)
        {
            if (_AttackedObjects.Contains(attk)) continue;
            _AttackedObjects.Add(attk);
            attk.ApplyDamage(_Damage);
        }
    }
}
