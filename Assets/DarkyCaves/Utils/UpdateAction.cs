﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>A component that will perform an action every update over a period of time.
/// Once the period of time has expired, or Cancel is called. It will invoke the finishaction</summary>
public class UpdateAction : MonoBehaviour
{
    /// <summary>This action is called every update, with the given T along the lerp given</summary>
    private Action<float> _UpdateTAction;
    /// <summary>When the totaltime lapse has been reached then invoke this action and destroy this component</summary>
    private Action _FinishAction;

    /// <summary>The total time taken for this updateaction</summary>
    private TimeSpan _TotalTimeTaken;

    private TimeSpan _CurrentTimeTaken;

    private bool _Playing;

    public void Play(Action<float> updateTAction, TimeSpan totalTimeTaken, Action finishAction)
    {
        _UpdateTAction = updateTAction;
        _TotalTimeTaken = totalTimeTaken;
        _CurrentTimeTaken = TimeSpan.Zero;
        _FinishAction = finishAction;
        _Playing = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_Playing) return;
        _CurrentTimeTaken += TimeSpan.FromSeconds(Time.deltaTime);
        if (_CurrentTimeTaken > _TotalTimeTaken) _CurrentTimeTaken = _TotalTimeTaken;
        //Work out the progress ratio for how long the animation has been going, 0 (just started) and 1 ( finished)
        //Pass this into the update action to be allowed to lerp on the recieving end
        _UpdateTAction.Invoke((float)_CurrentTimeTaken.TotalSeconds / (float)_TotalTimeTaken.TotalSeconds);
        if (_CurrentTimeTaken < _TotalTimeTaken) return;
        Cancel(false);
    }

    public void Cancel(bool triggerFinishAction)
    {
        _Playing = false;
        if (triggerFinishAction) _FinishAction.Invoke();
        Destroy(this);
    }
}
