﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerConstants
{
    /// <summary>Returns the layer mask for all of the obstacles which are not characters</summary>
    /// <returns></returns>
    public static int ObstaclesLayerMask()
    {
        return LayerMask.GetMask(new[] { "Obstacles" });
    }

    /// <summary>Returns the layer mask for all things that the player is, and holds or owns
    /// Use this if you want to ignore anything to do with colliding with the player</summary>
    /// <returns></returns>
    public static int PlayerLayerMask()
    {
        return LayerMask.GetMask(new[] { "Player", "PlayerWeapons" });
    }
}
