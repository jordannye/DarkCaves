﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class WeightedRandom
{
    /// <summary>
    /// The 2 arrays have to be the same length.
    /// 
    /// </summary>
    /// <param name="weightings"></param>
    /// <param name="choices"></param>
    /// <returns></returns>
    public static int ReturnRandomFromWeightings(int[] weightings, int[] choices )
    {
       // Debug.AssertFormat(weightings.Length != choices.Length,"both the choices and weightings need to contain the same amount of options");
        //Debug.AssertFormat(weightings.Length <= 1 || choices.Length <= 1, "You need more than 1 option when asking for weighted random");

        List<int> pool = new List<int>();

        //Add the choice ot the pool by the amount there are in the weightings.
        for (int i = 0; i < weightings.Length; i++)
        {
            //Add one choice per weighting to the prize pool
            for (int j = 0; j < weightings[i]; j++)
            {
                pool.Add(choices[i]);
            }
        }

        pool.Shuffle();

        return pool[UnityEngine.Random.Range(0, pool.Count)];
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0,n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
