﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkyUtils
{
    public static float Lerp(float firstFloat, float secondFloat, float by)
    {
        return firstFloat * by + secondFloat * (1 - by);
    }
}
