﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>Creates a list of buttons from FistingPowers which you can currently equip, based on whether or not you currently have them in your ActiveFistingPowers
/// table or not</summary>
public class EquippableHandAbilitiesToSelectFrom : UIVerticalList
{
    public RoomAndPassiveUIController ViewRoomController;
    public FistingPowerSelectionController FistingPowerSelectionController;
    public UITabManager HandTabs;
    public PowerConsumptionDisplay PowerConsumptionDisplay;

    protected override List<string> PopulateInformationList()
    {
        ExtraBufferBetweenItems = 10f;
        ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();
        FistingPowerSelectionController = FindObjectOfType<FistingPowerSelectionController>();
        PowerConsumptionDisplay = FindObjectOfType<PowerConsumptionDisplay>();
        GameObject tab = GameObject.Find("Tabs");
        if (tab != null) HandTabs = tab.GetComponent<UITabManager>();
        return UpdatePossibleFistingPowers();
    }

    public List<string> UpdatePossibleFistingPowers()
    {
        if (ViewRoomController.SelectedPlayerText == null) return new List<string>();

        string playerName = ViewRoomController.SelectedPlayerText.GetComponent<Text>().text;

        var unlockedFistingPowers = SQLPlayerInfo.SQLManager.Instance.GetPlayerUnlockedFistingPowers(playerName);
        var currentFistingPowers = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveFistingPowers(playerName);

        PowerConsumptionDisplay.UpdatePowerConsumptionAmount(playerName);
        float currentPowerConsumptionLevel = PowerConsumptionDisplay.CurrentPowerConsumptionLevel;

        List<string> toShow = new List<string>();
        foreach (var power in unlockedFistingPowers)
        {
            //If you already have this passive equipped, and you aren't allowed multiple of these passives equipped then dont show it for equipping
            if (currentFistingPowers.Contains(power)) continue;
            Component fistingPower = gameObject.AddComponent(AbilityUtils.FistingPowerType(power));
            float compPowerConsumptionLvl = fistingPower.GetComponentInChildren<IThrowableItem>().PowerPercentageConsumption;

            //Don't add the ability to the list if, when you were to equip it, it would put you over 100%
            if (currentPowerConsumptionLevel + compPowerConsumptionLvl > 100) continue;
            toShow.Add(string.Format("{0} [{1}%]", power, compPowerConsumptionLvl));
            Destroy(fistingPower);
        }
        InformationToShow = toShow;
        return toShow;
    }

    protected override void RoomButtonPressed(GameObject selectedGameObject)
    {
        string fistingPowerName = selectedGameObject.GetComponentInChildren<Text>().text;
        //We know from above, in the UpdatePossibleFistingPowers() that we are formatting it with a " [NUMBER%]" so just remove that from the end before saving it
        int index = 0;
        for (int i = fistingPowerName.Length - 1; i >= 0; i--)
        {
            if (fistingPowerName[i] != '[') continue;
            index = i - 1;
            break;
        }
        fistingPowerName = fistingPowerName.Substring(0, index);

        FistingPowerSelectionController.AttemptAddFistingPowerToHand(ViewRoomController.CurrentSelectedPlayerName, fistingPowerName);
        ViewRoomController.ShowHandAbilitiesSelectionScreen(false);
        ViewRoomController.ShowHandAbilitiesSelectionScreen(true);
    }
}
