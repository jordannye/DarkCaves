﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerConsumptionDisplay : MonoBehaviour
{
    private Text _PercentageToDisplay;
    protected Text PercentageToDisplay
    {
        get { if (_PercentageToDisplay == null) { _PercentageToDisplay = GetComponent<Text>(); } return _PercentageToDisplay; }
        set { _PercentageToDisplay = value; }
    }

    private float _TotalPercentageConsumption;

    // Use this for initialization
    void OnEnable()
    {
        _PercentageToDisplay = GetComponent<Text>();
    }

    public void UpdatePowerConsumptionAmount(string playerName)
    {
        var allActiveFistingPowers = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveFistingPowers(playerName);

        _TotalPercentageConsumption = 0f;
        if (allActiveFistingPowers != null)
        {
            foreach (string fistingPower in allActiveFistingPowers)
            {
                Component comp = gameObject.AddComponent(AbilityUtils.FistingPowerType(fistingPower));
                if (comp.GetComponent<IThrowableItem>() != null)
                {
                    _TotalPercentageConsumption += comp.GetComponent<IThrowableItem>().PowerPercentageConsumption;
                    Destroy(comp);
                }
            }
        }
        UpdateConsumptionVisual(_TotalPercentageConsumption);
    }

    protected void UpdateConsumptionVisual(float consumptionTotal)
    {
        PercentageToDisplay.text = string.Format("Power Consumption: {0}%", consumptionTotal);
    }

    public float CurrentPowerConsumptionLevel
    {
        get { return _TotalPercentageConsumption; }
    }
}
