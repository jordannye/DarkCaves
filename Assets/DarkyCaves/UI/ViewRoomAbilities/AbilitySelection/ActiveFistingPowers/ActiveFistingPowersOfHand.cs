﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>The scrollable list of buttons, showing the current active powers equipped to the players Left or Right hand</summary>
public class ActiveFistingPowersOfHand : UIVerticalList
{
    public RoomAndPassiveUIController ViewRoomController;
    public FistingPowerSelectionController FistingPowerSelectionController;

    protected override List<string> PopulateInformationList()
    {
        ExtraBufferBetweenItems = 10f;
        ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();
        FistingPowerSelectionController = FindObjectOfType<FistingPowerSelectionController>();
        return UpdatePossiblePowers();
    }

    public List<string> UpdatePossiblePowers()
    {
        if (ViewRoomController.SelectedPlayerText == null) return new List<string>();

        string playerName = ViewRoomController.SelectedPlayerText.GetComponent<Text>().text;

        var currentPower = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveFistingPowers(playerName);

        List<string> toShow = currentPower.ToList();
        return toShow;
    }

    protected override void RoomButtonPressed(GameObject selectedGameObject)
    {
        string fistingPowerName = selectedGameObject.GetComponentInChildren<Text>().text;

        FistingPowerSelectionController.AttemptRemoveFistingPowerFromHand(ViewRoomController.CurrentSelectedPlayerName, fistingPowerName);
        ViewRoomController.ShowHandAbilitiesSelectionScreen(false);
        ViewRoomController.ShowHandAbilitiesSelectionScreen(true);
    }
}
