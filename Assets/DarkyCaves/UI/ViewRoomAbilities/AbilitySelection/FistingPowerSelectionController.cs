﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>Controls the process of attempting to add FistingPowers from the UnlockedFistingPowers table, into the ActiveFistingPowers table.
/// You cannot have the same power equip on either hand at the same time, and your total power consumption cannot add up to over 100% when all powers have been equipped</summary>
public class FistingPowerSelectionController : MonoBehaviour
{
    public RoomAndPassiveUIController ViewRoomController;
    public PowerConsumptionDisplay PowerConsumptionDisplay;

    private void OnEnable()
    {
        ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();
        PowerConsumptionDisplay = FindObjectOfType<PowerConsumptionDisplay>();
    }

    /// <summary>Work out if you can add this fisting power, onto this hand without exceeding the 100% power consumed mark, AND if it hasn't already been added to either hand</summary>
    /// <param name="fistingPowerName"></param>
    /// <param name="handName"></param>
    public void AttemptAddFistingPowerToHand(string playerName, string fistingPowerName)
    {
        var allActiveFistingPowers = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveFistingPowers(playerName);

        if (allActiveFistingPowers.Contains(fistingPowerName)) return;

        float currentTotalPowerConsumption = 0f;
        foreach (string fistingPower in allActiveFistingPowers)
        {
            Component comp = gameObject.AddComponent(AbilityUtils.FistingPowerType(fistingPower));
            if (comp.GetComponent<IThrowableItem>() != null)
            {
                currentTotalPowerConsumption += comp.GetComponent<IThrowableItem>().PowerPercentageConsumption;
            }
            Destroy(comp);
        }

        if (currentTotalPowerConsumption > 100) return;

        SQLPlayerInfo.SQLManager.Instance.AddFistingPower(playerName, fistingPowerName);

        PowerConsumptionDisplay.UpdatePowerConsumptionAmount(playerName);
    }

    public void AttemptRemoveFistingPowerFromHand(string playerName, string fistingPowerName)
    {
        SQLPlayerInfo.SQLManager.Instance.RemoveFistingPower(playerName, fistingPowerName);
        PowerConsumptionDisplay.UpdatePowerConsumptionAmount(playerName);
    }

}
