﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatStrengthsToText : MonoBehaviour
{
    private Text _TextField;
    private StatSelectionScreen _StatSelectionScreen;

    /// <summary>Update the Text showing each stats extra benefits for the player, ignoring their default ones</summary>
    public void UpdateStatStrengthInfo()
    {
        if (_TextField == null) _TextField = GetComponent<Text>();
        if (_StatSelectionScreen == null) _StatSelectionScreen = FindObjectOfType<StatSelectionScreen>();

        string textToShow = "";

        foreach (var pt in _StatSelectionScreen.PointTypes)
        {
            if (pt.Name.Equals("Health"))
            {
                textToShow += string.Format("Health creating {0} CC Resistance\n\n", CharacterStats.ConvertHealthToCCRes(pt.Current).ToString("N1"));
            }
            else if (pt.Name.Equals("Shield"))
            {
                textToShow += string.Format("Shield creating {0} Movement speed\n\n", CharacterStats.ConvertShieldToMS(pt.Current).ToString("N1"));
            }
            else if (pt.Name.Equals("Damage"))
            {
                textToShow += string.Format("Damage creating {0} Throw Distance\n", CharacterStats.ConvertDamageToThrowDistance(pt.Current).ToString("N1"));
                textToShow += string.Format("Damage also creating {0} Throw Speed\n", CharacterStats.ConvertDamageToThrowSpeed(pt.Current).ToString("N1"));
            }
        }

        _TextField.text = textToShow;
    }
}
