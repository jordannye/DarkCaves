﻿using Glide;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleStatsSlider : MonoBehaviour
{
    private StatSelectionScreen _StatSelectionScreen;

    private void OnEnable()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>Called by the Slider component when the slider has changed value</summary>
    /// <param name="current"></param>
    public void ValueChange(float current)
    {
        if (StatSelectionScreen.PauseValueUpdates) return;

        if (_StatSelectionScreen == null) _StatSelectionScreen = FindObjectOfType<StatSelectionScreen>();
        //Now the current value IS the max value, so update the visual to be the current/max value (fill up the visual slider to its max value)
        _StatSelectionScreen.UpdateStatValue(transform.parent.gameObject.name, (int)current);
    }
}
