﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon;
using System.Linq;

public class RoomBrowser : UIVerticalList
{
    private List<string> RoomNames;

    public override void Start()
    {
        base.Start();
        InvokeRepeating("RefreshRoomInfo", 2f, 2f);
    }

    protected void RefreshRoomInfo()
    {
        InformationToShow = PopulateInformationList();
        CreateObjectList();
    }

    // Use this for initialization
    protected override List<string> PopulateInformationList()
    {
        RoomInfo[] NewRoomList = PhotonNetwork.GetRoomList();

        List<string> roomInfo = new List<string>();
        RoomNames = new List<string>();

        for (int i = 0; i < NewRoomList.Length; i++)
        {
            if (!NewRoomList[i].IsOpen) continue;
            string roomName = NewRoomList[i].Name;
            int roomPlayers = NewRoomList[i].PlayerCount;
            string totalPlayersAllowed = NewRoomList[i].MaxPlayers.ToString();
            roomInfo.Add(roomName + "\n" + "Players : " + roomPlayers.ToString() + "/" + totalPlayersAllowed);
            RoomNames.Add(roomName);
        }

        return roomInfo;
    }

    protected override void CreateObjectList()
    {
        base.CreateObjectList();
        for (int i = 0; i < _CurrentListOfObjects.Length; i++)
        {
            _CurrentListOfObjects[i].GetComponent<RoomButtonInformation>().RoomName = RoomNames[i];
        }
    }

    private RoomInfo _LoadRoomInfo;

    protected override void RoomButtonPressed(GameObject selectedGameObject)
    {
        string roomName = selectedGameObject.GetComponent<RoomButtonInformation>().RoomName;
        RoomInfo room = PhotonNetwork.GetRoomList().First(s => s.Name.Equals(roomName));
        if (room != null && !room.IsOpen)
        {
            FindObjectOfType<RoomAndPassiveUIController>().ErrorMessageText.GetComponent<Text>().text = "You cannot join this room, it isn't open anymore!";
            return;
        }
        PhotonNetwork.JoinRoom(roomName);
        CancelInvoke();
    }
}
