﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon;

public class CreateRoomController : Photon.PunBehaviour
{
    private RoomAndPassiveUIController _ViewRoomController;

    /// <summary>Button to request to create a new room, using the Text in the RoomNameInputField</summary>
    private Button _CreateRoomButton;
    /// <summary>User name input for a new room</summary>
    private InputField _RoomNameInputField;

    /// <summary>Error message output when a room is trying to be created but fails</summary>
    private Text _RoomCreationMessage;

    public string RoomNameToCreate;

    public string SceneNameToLoadInto = "Hub_World";

    private bool _AttemptingCreateRoom = false;

    // Use this for initialization
    void Start()
    {
        _ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();
        _CreateRoomButton = transform.FindChild("CreateRoomButton").GetComponent<Button>();
        _RoomNameInputField = transform.FindChild("InputField").GetComponent<InputField>();
        _RoomCreationMessage = GameObject.Find("ErrorMessage").GetComponent<Text>();

        RoomNameToCreate = "TestRoom004";

        _RoomNameInputField.onValueChanged.AddListener((roomName) =>
       {
           RoomNameToCreate = roomName;
       });

        _CreateRoomButton.onClick.AddListener(() =>
        {
            if (_AttemptingCreateRoom) return;
            _AttemptingCreateRoom = AttemptCreateRoom();
            if (!_AttemptingCreateRoom) return;
            MakeAndJoinRoom();
        });
    }

    private bool AttemptCreateRoom()
    {
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        if (RoomNameToCreate == null || RoomNameToCreate == "")
        {
            _RoomCreationMessage.text = "You cannot create an unnamed room, please type in a room name";
            return false;
        }
        foreach (RoomInfo singleRoom in rooms)
        {
            if (singleRoom.Name.Equals(RoomNameToCreate))
            {
                _RoomCreationMessage.text = RoomNameToCreate + " already exists, choose another room name";
                return false;
            }
        }
        return true;
    }

    private void MakeAndJoinRoom()
    {
        _ViewRoomController.EnterGame();

        ExitGames.Client.Photon.Hashtable roomInfo = new ExitGames.Client.Photon.Hashtable
        {
            { "SceneName", SceneNameToLoadInto }
        };

        PhotonNetwork.JoinOrCreateRoom(RoomNameToCreate, new RoomOptions()
        {
            MaxPlayers = 10,
            CustomRoomProperties = roomInfo
        }, TypedLobby.Default);

        //Now the RoomBrowser will handle joining and loading into this room with the selected "SceneName" provided
    }

    public override void OnCreatedRoom()
    {
        _RoomCreationMessage.text = RoomNameToCreate + " room successfully created";
    }
}
