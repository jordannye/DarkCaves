﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EquippablePassivesToSelectFrom : UIVerticalList
{
    public PassiveEquipScreen PassiveEquipScreen;
    public PassiveSelectionScreenController PassiveSelectionScreen;
    public RoomAndPassiveUIController ViewRoomController;

    protected override List<string> PopulateInformationList()
    {
        ExtraBufferBetweenItems = 10f;
        ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();
        PassiveEquipScreen = FindObjectOfType<PassiveEquipScreen>();
        PassiveSelectionScreen = ViewRoomController.PassiveSelectionScreen.GetComponent<PassiveSelectionScreenController>();
        return UpdatePossiblePassives();
    }

    public List<string> UpdatePossiblePassives()
    {
        string playerName = ViewRoomController.SelectedPlayerText.GetComponent<Text>().text;

        List<string> passiveInfo = SQLPlayerInfo.SQLManager.Instance.GetPlayerUnlockedPassives(playerName).ToList();
        List<string> currentPassives = SQLPlayerInfo.SQLManager.Instance.GetActivePassives(playerName).ToList();

        List<string> toShow = new List<string>();
        for (int i = 0; i < passiveInfo.Count; i++)
        {
            //If you already have this passive equipped, and you aren't allowed multiple of these passives equipped then dont show it for equipping
            if (currentPassives.Contains(passiveInfo[i]) && PassiveNotAllowedMultiple(passiveInfo[i])) continue;
            toShow.Add(passiveInfo[i]);
        }
        InformationToShow = toShow;
        return toShow;
    }

    /// <summary>Return true if you are only allowed 1 of this passive equipped at once</summary>
    /// <param name="passiveName"></param>
    /// <returns></returns>
    private bool PassiveNotAllowedMultiple(string passiveName)
    {
        return passiveName != AbilityUtils.PassiveNames.DefaultWaitPassive;
    }

    protected override void RoomButtonPressed(GameObject selectedGameObject)
    {
        string passiveName = selectedGameObject.GetComponentInChildren<Text>().text;
        if (SQLPlayerInfo.SQLManager.Instance.UpdatePassiveInfo(ViewRoomController.CurrentSelectedPlayerName, passiveName, PassiveEquipScreen.PassiveIndexSelected))
        {
            PassiveSelectionScreen.UpdatePassiveButtonText();
            ViewRoomController.ErrorMessageText.GetComponent<Text>().text = string.Format("Passive{0} has been updated to be {1}", PassiveEquipScreen.PassiveIndexSelected + 1, passiveName);
        }
        else
        {
            ViewRoomController.ErrorMessageText.GetComponent<Text>().text = string.Format("ERROR: There was an error and Passive{1} has NOT been updated to be {1}", PassiveEquipScreen.PassiveIndexSelected + 1, passiveName);
        }
        PassiveEquipScreen.OnBackPressed();
    }
}
