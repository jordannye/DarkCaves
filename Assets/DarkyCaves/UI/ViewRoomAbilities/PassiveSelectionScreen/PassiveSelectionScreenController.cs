﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PassiveSelectionScreenController : MonoBehaviour
{
    /// <summary>The text that stores the current player the User has selected to play</summary>
    public GameObject PlayerSelected;
    /// <summary>The Passive buttons, that are contained within the PassiveSelectionScreen, which when pressed will open up the PassiveEquip screen</summary>
    public GameObject[] PassiveSelectionButtons;
    /// <summary>The button used to close this screen</summary>
    public GameObject PassiveSelectionBackButton;

    /// <summary>The passive equip screen will be a list of UnlockedPassives from the database, that have been unlocked by this PlayerName
    /// This will be stored in the database Table UnlockedPassives</summary>
    public GameObject PassiveEquipScreen;

    /// <summary>Back button to go out of the passive equip screen</summary>
    public GameObject PassiveEquipScreenBackButton;

    public string PassiveSlotSelected = "";

    // Use this for initialization
    void OnEnable()
    {
        if (PassiveSelectionBackButton != null)
        {
            UpdatePassiveButtonText();
            return;
        }

        PlayerSelected = GameObject.Find("PlayerSelected");

        UpdatePassiveButtonText();

        PassiveSelectionBackButton = GameObject.Find("PassiveSelectionScreenBackButton");

        PassiveEquipScreen = FindObjectOfType<RoomAndPassiveUIController>().PassiveEquipScreen;

        PassiveEquipScreenBackButton = GameObject.Find("PassiveEquipScreenBackButton");

        UpdatePassiveButtonText();
    }

    public void UpdatePassiveButtonText()
    {
        List<string> passivesActive = SQLPlayerInfo.SQLManager.Instance.GetActivePassives(PlayerSelected.GetComponent<Text>().text).ToList();

        PassiveSelectionButtons = new GameObject[SQLPlayerInfo.SQLManager.PlayerPassiveMax];
        for (int i = 0; i < PassiveSelectionButtons.Length; i++)
        {
            PassiveSelectionButtons[i] = transform.GetChild(i).gameObject;
            if (i >= passivesActive.Count || passivesActive[i] == null)
            {
                PassiveSelectionButtons[i].GetComponentInChildren<Text>().text = "Passive " + (i + 1);
            }
            else
            {
                PassiveSelectionButtons[i].GetComponentInChildren<Text>().text = passivesActive[i];
            }
        }
    }

    public void PassiveButtonClicked(int index)
    {
        PassiveSlotSelected = string.Format("Passive{0}", index);

        PassiveEquipScreen.SetActive(true);
        PassiveEquipScreen.GetComponent<PassiveEquipScreen>().PassiveIndexSelected = index;

        gameObject.SetActive(false);
    }

    public void ExitPassiveEquipScreen()
    {
        PassiveEquipScreen.SetActive(false);
    }

    private void OnDisable()
    {

    }

}
