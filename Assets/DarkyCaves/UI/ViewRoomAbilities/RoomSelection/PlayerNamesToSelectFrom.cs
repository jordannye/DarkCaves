﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNamesToSelectFrom : UIVerticalList
{
    protected override List<string> PopulateInformationList()
    {
        return SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().ToList();
    }

    private void OnEnable()
    {
        UpdatePlayerNames();
    }

    public void UpdatePlayerNames()
    {
        InformationToShow = SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().ToList();
        CreateObjectList();
    }

    protected override void RoomButtonPressed(GameObject selectedGameObject)
    {
        base.RoomButtonPressed(selectedGameObject);
        FindObjectOfType<RoomAndPassiveUIController>().PlayerSelected(selectedGameObject.GetComponentInChildren<Text>().text);
    }
}
