﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ExpandUIButton : ExpandRectTransform
{
    protected Button _Button;

    protected override void ExtendedStart()
    {
        _Button = GetComponent<Button>();
        _Button.onClick.AddListener(ToggleResizing);
    }
}
