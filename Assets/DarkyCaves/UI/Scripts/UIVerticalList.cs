﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVerticalList : MonoBehaviour
{
    public GameObject ElementPrefab;

    protected GameObject[] _CurrentListOfObjects;

    public List<string> InformationToShow;

    protected float ExtraBufferBetweenItems = 0f;

    private void OnEnable()
    {
        Start();
    }

    // Use this for initialization
    public virtual void Start()
    {
        InformationToShow = PopulateInformationList();
        CreateObjectList();
    }

    protected virtual List<string> PopulateInformationList()
    {
        return new List<string>();
    }

    protected virtual void CreateObjectList()
    {
        DeleteList();

        int amount = InformationToShow.Count;
        _CurrentListOfObjects = new GameObject[amount];

        for (int i = 0; i < amount; i++)
        {
            _CurrentListOfObjects[i] = Instantiate(ElementPrefab);
            RectTransform oldRect = _CurrentListOfObjects[i].GetComponent<RectTransform>();
            _CurrentListOfObjects[i].transform.SetParent(transform);
            _CurrentListOfObjects[i].transform.localPosition = Vector3.zero;
            _CurrentListOfObjects[i].transform.localScale = Vector3.one;
            RectTransform rectT = _CurrentListOfObjects[i].GetComponent<RectTransform>();
            rectT.anchoredPosition = new Vector2(0, -((rectT.rect.height * 1.35f) * i) - ExtraBufferBetweenItems);

            Button button = _CurrentListOfObjects[i].GetComponentInChildren<Button>();
            int index = i;
            button.onClick.AddListener(delegate
            {
                CancelInvoke();
                RoomButtonPressed(_CurrentListOfObjects[index]);
            });

            button.GetComponentInChildren<Text>().text = InformationToShow[i];
        }

        if (_CurrentListOfObjects == null || _CurrentListOfObjects.Length <= 0) return;
        float objHeight = _CurrentListOfObjects[0].GetComponent<RectTransform>().rect.height * 1.35f;
        Rect rect = GetComponentInParent<RectTransform>().rect;
        GetComponent<RectTransform>().sizeDelta = new Vector2(rect.width, (objHeight * _CurrentListOfObjects.Length) + (_CurrentListOfObjects.Length * ExtraBufferBetweenItems));
    }

    protected virtual void RoomButtonPressed(GameObject selectedGameObject)
    {

    }

    private void DeleteList()
    {
        if (_CurrentListOfObjects == null) return;
        foreach (GameObject obj in _CurrentListOfObjects)
        {
            Destroy(obj);
        }
        _CurrentListOfObjects = null;
    }
}
