﻿using DarkyEvents;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class HealthInformation : MonoBehaviour
{
    private ProgressBar _ShieldBar { get { return transform.GetChild(0).GetComponent<ProgressBar>(); } }
    private ProgressBar _HealthBar { get { return transform.GetChild(1).GetComponent<ProgressBar>(); } }

    private void OnEnable()
    {
        DarkyEvents.EventManager.StartListening(EventNames.HealthFirstInitialised, this.HealthInit);
        DarkyEvents.EventManager.StartListening(EventNames.CharacterDamaged, this.DamageTaken);
        DarkyEvents.EventManager.StartListening(EventNames.RechargeShieldStart, this.ShieldRechargeStart);
    }

    private void OnDisable()
    {
        DarkyEvents.EventManager.StopListening(EventNames.HealthFirstInitialised, this.HealthInit);
        DarkyEvents.EventManager.StopListening(EventNames.CharacterDamaged, this.DamageTaken);
        DarkyEvents.EventManager.StopListening(EventNames.RechargeShieldStart, this.ShieldRechargeStart);
    }

    // Use this for initialization
    void Start()
    {
    }

    private void ApplyHealthUpdateVisual(HealthType healthType, float updateTime)
    {
        _ShieldBar.SetBarValue(healthType.Shield, updateTime);
        _HealthBar.SetBarValue(healthType.Health, updateTime);
    }

    private void HealthInit(System.Object healthType)
    {
        if (healthType == null) return;
        HealthType ht = (HealthType)healthType;
        _ShieldBar.SetBarValue(ht.Shield, 2);
        _HealthBar.SetBarValue(ht.Health, 2);
    }

    private void ShieldRechargeStart(System.Object shieldData)
    {
        object maxShieldValue = shieldData.ValFromObject("MaxShield");
        object shieldRechargeTime = shieldData.ValFromObject("ShieldRechargeTime");

        _ShieldBar.SetBarValue((float)maxShieldValue, (float)shieldRechargeTime);
    }

    private void DamageTaken(System.Object damageTyperHealthType)
    {
        object damageType = damageTyperHealthType.ValFromObject("DamageType");
        object healthType = damageTyperHealthType.ValFromObject("HealthType");

        ApplyHealthUpdateVisual((HealthType)healthType, 0f);
    }
}
