﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PassiveUIItem : MonoBehaviour
{
    private Image[] _Images;
    private Text[] _Texts;
    // Use this for initialization
    void Start()
    {
        _Images = GetComponentsInChildren<Image>();
        _Texts = GetComponentsInChildren<Text>();
    }

    private float _Alpha;
    public float Alpha
    {
        get { return _Alpha; }
        set
        {
            _Alpha = value;
            foreach (Image img in _Images) { img.color = new Color(img.color.r, img.color.g, img.color.b, _Alpha); }
            foreach (Text txt in _Texts) { txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, _Alpha); }
        }
    }
}
