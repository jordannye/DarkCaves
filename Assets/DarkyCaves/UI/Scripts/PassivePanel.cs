﻿using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Glide;
using SQLPlayerInfo;
using System.Linq;
using DarkyEvents;

[RequireComponent(typeof(RectTransform))]
public class PassivePanel : MonoBehaviour
{
    public static PassivePanel Instance;

    private RectTransform _RectTransform;

    /// <summary>The prefab used for each item</summary>
    public GameObject ItemPrefab;

    /// <summary>none "selected" items will have this scale applied to them</summary>
    public float ScaleForUnselectedItem = 0.7f;

    /// <summary>The index of the ListItems which will be the "selected" item</summary>
    public int IndexOfSelectedItem = 0;

    public float DistanceBetween = 129f;

    /// <summary>The current list of items to show</summary>
    public List<GameObject> ListItems;

    private Tweener _Tweener = new Tweener();

    public float PassiveIconMoveSpeed = 0.3f;

    void OnEnable()
    {
        DarkyEvents.EventManager.StartListening(EventNames.PassiveActivated, this.UpdatePassiveScales);
        DarkyEvents.EventManager.StartListening(EventNames.PassiveAdded, this.UpdatePassiveInformation);
    }
    private void OnDisable()
    {
        DarkyEvents.EventManager.StopListening(EventNames.PassiveActivated, this.UpdatePassiveScales);
        DarkyEvents.EventManager.StopListening(EventNames.PassiveAdded, this.UpdatePassiveInformation);
    }

    private void Awake()
    {
        if (Instance != null) Destroy(this);
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        if (ListItems == null || ListItems.Count == 0) CreateUI();
    }

    private Vector2 CalculateUIPosition(int index)
    {
        return _RectTransform.anchoredPosition + new Vector2(index * DistanceBetween, 0f);
    }

    private void CreateUI()
    {
        _RectTransform = GetComponent<RectTransform>();

        ListItems = new List<GameObject>();
        int count = SQLManager.Instance.GetActivePassives(PhotonNetwork.playerName).Count();
        for (int i = 0; i < count; i++)
        {
            GameObject lItem = Instantiate(ItemPrefab);
            lItem.transform.SetParent(transform);
            lItem.transform.localScale = Vector3.one;
            RectTransform rectT = lItem.GetComponent<RectTransform>();
            rectT.localPosition = Vector3.zero;
            rectT.anchoredPosition = CalculateUIPosition(i);
            ListItems.Add(lItem);
        }
        IndexOfSelectedItem = 0;
        UpdatePassiveScales(IndexOfSelectedItem);
    }

    public void UpdatePassiveScales(System.Object newIndex)
    {
        _Tweener.CancelAndComplete();
        int activeIndex = (int)newIndex;
        //Loop for the amount of items in the passives array, but start at the index that is now active
        for (int i = 0; i < ListItems.Count; i++)
        {
            int currentPassiveIndex = (activeIndex + i) % ListItems.Count;

            RectTransform rectT = ListItems[currentPassiveIndex].GetComponent<RectTransform>();

            bool active = currentPassiveIndex == (int)newIndex;
            bool last = i == ListItems.Count - 1;
            Vector2 extra = last ? new Vector2(DistanceBetween, 0) : Vector2.zero;
            RectTransform rectT1 = rectT;
            int i1 = i;

            _Tweener.Tween(rectT1, new { anchoredPosition = CalculateUIPosition(last ? 0 : i1) - extra }, PassiveIconMoveSpeed);
            _Tweener.Tween(rectT1, new { localScale = Vector3.one * (currentPassiveIndex == (int)newIndex ? 1 : ScaleForUnselectedItem) },
            PassiveIconMoveSpeed).OnComplete(() => { rectT1.anchoredPosition = CalculateUIPosition(i1); });

            PassiveUIItem pitem = rectT1.gameObject.GetComponent<PassiveUIItem>();
            _Tweener.Tween(pitem, new { Alpha = active || i1 < 3 ? 1f : 0f }, PassiveIconMoveSpeed * 0.6f);
        }
    }

    public void UpdatePassiveInformation(System.Object passiveInfo)
    {
        if (ListItems == null || ListItems.Count == 0) CreateUI();

        object PassiveIndex = passiveInfo.ValFromObject("Index");
        object Passive = passiveInfo.ValFromObject("Passive");

        if (PassiveIndex == null || Passive == null /*|| (int)PassiveIndex >= ListItems.Count*/) return;

        ListItems[(int)PassiveIndex].GetComponentInChildren<Text>().text = Passive.ToString();
    }

    private void Update()
    {
        _Tweener.Update(Time.deltaTime);
    }
}
