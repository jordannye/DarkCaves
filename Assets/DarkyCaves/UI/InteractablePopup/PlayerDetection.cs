﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class OnPlayerEnter : UnityEvent { }
[Serializable]
public class OnPlayerExit : UnityEvent { }

public class PlayerDetection : MonoBehaviour
{
    public OnPlayerEnter PlayerEnterEvent;
    public OnPlayerExit PlayerExitEvent;

    private CharacterMainComponent _CharMain;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        CharacterMainComponent charMain = collider.gameObject.GetComponent<CharacterMainComponent>();
        if (charMain == null || !charMain.photonView.isMine) return;
        _CharMain = charMain;
        if(PlayerEnterEvent != null) PlayerEnterEvent.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        CharacterMainComponent charMain = collider.gameObject.GetComponent<CharacterMainComponent>();
        if (_CharMain == null || !_CharMain.Equals(charMain)) return;
        _CharMain = null;
        if (PlayerExitEvent != null) PlayerExitEvent.Invoke();
    }
}
