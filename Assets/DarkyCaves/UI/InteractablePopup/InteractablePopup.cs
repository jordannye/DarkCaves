﻿using Glide;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class InteractablePopup : MonoBehaviour
{
    public float MaxScaleTo = 0.7f;

    private Tweener _Tweener = new Tweener();

    private SpriteRenderer _Sprite;
    private SpriteRenderer SpriteRenderer
    {
        get
        {
            if (_Sprite == null) _Sprite = GetComponent<SpriteRenderer>();
            return _Sprite;
        }
        set { _Sprite = value; }
    }

    public void StartPopup()
    {
        _Tweener.Cancel();
        _Tweener.Tween(SpriteRenderer.transform, new { localScale = Vector3.one }, 0.4f).OnComplete(() =>
        {
            _Tweener.Tween(SpriteRenderer.transform, new { localScale = new Vector3(MaxScaleTo, MaxScaleTo, 1f) }, 0.4f).Repeat().Reflect();
        });
    }

    public void EndPopup()
    {
        _Tweener.Cancel();
        _Tweener.Tween(SpriteRenderer.transform, new { localScale = new Vector3(0f, 0f, 1f) }, 0.4f);
    }

    // Update is called once per frame
    void Update()
    {
        _Tweener.Update(Time.deltaTime);
    }
}
