﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(CircleCollider2D))]
public class DamageRadius_Debug : MonoBehaviour
{
    private SpriteRenderer _SpriteRenderer;
    private CircleCollider2D _CircleCollider;

    public float DamageAmount = 50f;
    public DamageType.ElementType DamageElementType;

    // Use this for initialization
    void Start()
    {
        _SpriteRenderer = GetComponent<SpriteRenderer>();
        _CircleCollider = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        foreach (Component comp in collision.gameObject.GetComponents<Component>())
        {
            if (!(comp is IAttackable)) continue;
            IAttackable attack = comp as IAttackable;
            attack.ApplyDamage(new DamageType() { DamageValue = DamageAmount * Time.deltaTime, ElementalType = DamageElementType });
        }
    }
}
