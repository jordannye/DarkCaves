﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingMousePos : MonoBehaviour
{
    public Transform FollowTransform;
    public Vector3 _MousePos;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        _MousePos = Camera.main.ScreenToWorldPoint(mousePos);
        _MousePos.z = FollowTransform.transform.position.z;
        FollowTransform.transform.position = _MousePos;
    }
}
